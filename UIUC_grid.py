from rungrid import *

file = open('jobs.txt','r')
lines = file.readlines()

lis1 = ['bronte.cs.illinois.edu']*3
lis2 = ['austen.cs.illinois.edu']*3
lis3 = ['shelley.cs.illinois.edu']*3
#lis4 = ['smaug.cs.illinois.edu']*0
lis4 = ['morgoth.cs.illinois.edu']*3

all_nodes = lis1 + lis2 + lis3 + lis4

jobs = []

for i in lines:
    #print i.strip()
    cmd=i.strip()
    print cmd
    jobs.append(cmd)

print jobs
print len(jobs)
grid = Grid(all_nodes,jobs)
grid.go()
grid = Grid(['bronte.cs.illinois.edu', 'austen.cs.illinois.edu', 'shelley.cs.illinois.edu','morgoth.cs.illinois.edu','smaug.cs.illinois.edu'], lines)
grid.go()
