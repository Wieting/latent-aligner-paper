package edu.illinois.cs.utils;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;
import edu.illinois.cs.readers.Example;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;

public class Utils {

	public static HashSet<String> stops = new HashSet<String>();
	public static HashMap<String,String> univ_pos = new HashMap<String,String>();

	public static boolean removePunctFromPPDB = false;

	public final static List<String> punctuationPOS = Arrays.asList("''",
			"``", ",", ":", ".", "-LRB-", "-RRB-", "-LCB-", "-RCB-", "-LSB-",
			"-RSB-");

	public static boolean isPunct(String s) {
		return punctuationPOS.contains(s);
	}

	public static String getUnivPOS(String pos) {
		if(univ_pos.size() == 0) {
			try {
				for (String line : LineIO.read(Constants.univ_pos)) {
					line = line.trim();
					if(line.length() > 0) {
						String[] arr = line.split("\\s");
						String penn = arr[0];
						String uni = arr[1];
						univ_pos.put(penn, uni);
					}
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return null;
			}
		}

		if(univ_pos.containsKey(pos))
			return univ_pos.get(pos);

		return pos;
	}

	public static String getCoarsePOS(String s) {
		if(s.toUpperCase().startsWith("N"))
			return "N";
		if(s.toUpperCase().startsWith("V"))
			return "V";
		if(s.toUpperCase().startsWith("R"))
			return "R";
		if(s.toUpperCase().startsWith("J"))
			return "J";

		return s;
	}

	public static boolean isStop(String s) {
		s = s.toLowerCase();
		if(stops.size() == 0)
			stops = loadStopWords();
		if(stops.contains(s))
			return true;
		return false;
	}

	public static HashSet<String> loadStopWords() {
		HashSet<String> stopwords = new HashSet<String>();

		try {
			for (String line : LineIO.read(Constants.stopwords)) {
				line = line.trim();
				if(line.length() > 0) {
					stopwords.add(line);
				}
			} 
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return stopwords;
	}

	public static HashMap<String, HashSet<String>> loadPPDB(String fname, StanfordCoreNLP pipeline) {
		HashMap<String,HashSet<String>> m = new HashMap<String,HashSet<String>>();

		try {
			for (String line : LineIO.read(fname)) {
				line = line.trim();
				if(line.length() > 0) {
					String[] arr = line.split("\\|\\|\\|");
					//System.out.println(arr.length+" "+line);
					if(arr.length != 2)
						continue;
					String p1 = arr[0];
					String p2 = arr[1];
					//System.out.println(p1+"\t"+p2);
					if(removePunctFromPPDB) {
						p1 = removePunct(pipeline, p1);
						p2 = removePunct(pipeline, p2);
					}
					int n = p1.split("\\s").length;
					int l = p2.split("\\s").length;
					if(n > Constants.max_phrase_length)
						Constants.max_phrase_length = n;
					if(l > Constants.max_phrase_length)
						Constants.max_phrase_length = l;
					if(m.containsKey(p1)) {
						m.get(p1).add(p2);
					}
					else {
						HashSet<String> temp = new HashSet<String>();
						temp.add(p2);
						m.put(p1, temp);
					}

					if(m.containsKey(p2)) {
						m.get(p2).add(p1);
					}
					else {
						HashSet<String> temp = new HashSet<String>();
						temp.add(p1);
						m.put(p2, temp);
					}
				}
			} 
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return m;
	}

	//todo - make more robust.
	public static String removePunct(StanfordCoreNLP pipeline, String s) {
		//System.out.println(s);
		//System.out.println(Utils.cleanup(s));
		//System.out.println(s.split("\\s+").length);
		if(s.split("\\s+").length == 1)
			return s;
		try {
			Annotation document = new Annotation(s);
			pipeline.annotate(document);
			List<CoreMap> sentences = document.get(SentencesAnnotation.class);
			CoreMap l_map = sentences.get(0);

			List<CoreLabel> toks = l_map.get(TokensAnnotation.class);

			String temp = "";
			for(CoreLabel c : toks) {
				String t = c.originalText();
				if(!Utils.isPunct(t)) {
					temp = temp+" "+t;
				}
			}
			return temp.trim();
		}
		catch(java.lang.IllegalStateException e) {
			return s;
		}
	}

	public static String cleanup(String string) {
		String clean = string.replaceAll("&quot;", "\"");
		clean = clean.replaceAll("&amp;","&");
		clean = clean.replaceAll("\\[","(");
		clean = clean.replaceAll("\\]",")");
		clean = clean.replaceAll("£","PND ");
		clean = clean.replaceAll("€","EUR ");
		// Unicode ― and — characters
		clean = clean.replaceAll("\u2014", "--");
		clean = clean.replaceAll("\u2015", "--");
		clean = clean.replaceAll("\u00BD", "1/2");
		// Unicode no-break space character‎
		clean = clean.replaceAll("\u00A0"," ");
		// Unicode ø character
		clean = clean.replaceAll("\u00F8", "o");
		// Unicode ° (degrees) character
		clean = clean.replaceAll("\u00B0", "o");
		clean = clean.replaceAll("\\. \\.", ".");
		clean = clean.replaceAll("\\? \\.", "?");
		clean = clean.replaceAll("\\.\\) \\.", ".)");
		clean = clean.replaceAll("\\.\\(", ". (");
		clean = clean.replaceAll("\\('", "( '");
		// Fix unicode diacritics
		clean = StringUtils.normalizeUnicodeDiacritics(clean);
		// A very simple URL catcher
		clean = clean.replaceAll("(https?://)?(www\\.)?[a-z]+\\.[a-z]+(/[a-z]+)?", "URL");
		return clean;
	}

	public static String getText(List<CoreLabel> s_toks, int start, int end) {

		String t = "";
		for(int i=start; i <= end; i++) {
			t = t + s_toks.get(i).originalText()+" ";
		}
		t = t.trim();

		return t;
	}

}
