package edu.illinois.cs.utils;

public class ConfigParams {

	public static final String LEARNER = "leanrer";
	public static final String ITER = "iter";
	public static final String CACHEFILE = "cachefile";
	public static final String FEATURESET = "featureset";
	public static final String READER = "reader";
	public static final String DEV = "dev";
	public static final String TRAIN = "train";
	public static final String TEST = "test";
	public static final String LOADFEATURECACHE = "loadfeaturecache";
	public static final String USEPPDB = "useppdb"; //for old features only
	public static final String USEWORDFEATURES = "usewordfeatures";
	public static final String PPDBPHRASE = "ppdbphrase";
	public static final String RANDOMINIT = "randominit";
	public static final String SERIALIZEDEXAMPLES = "serializedexamples";
	
	//set by command line
	public static boolean trim_training_data = false;
	public static boolean print_alignments = false;
	public static double R = 0.05;
}
