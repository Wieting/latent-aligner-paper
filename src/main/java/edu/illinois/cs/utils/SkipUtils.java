package edu.illinois.cs.utils;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

import edu.illinois.cs.cogcomp.core.io.LineIO;

public class SkipUtils extends ParagramUtils {
	
	public static HashMap<String,double[]> vectors = null;
	public static String unknown = "UUUNKKK";
	
	public static void skipwiki() {
		vectors = new HashMap<String,double[]>();
		
		try {
			for (String line : LineIO.read(Constants.skipwiki)) {
				line = line.trim();
				if(line.length() > 0) {
					String[] arr = line.split("\\s");
					String word = arr[0];
					double[] vec = new double[25];
					for(int i=1; i < arr.length; i++) {
						vec[i-1] = Double.parseDouble(arr[i]);
					}
					vectors.put(word, vec);
				}				
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static double paragramScore(String w1, String w2) {
		double[] d1 = getVector(w1);
		double[] d2 = getVector(w2);
		
		return cosine(d1,d2);
	}
	
	public static double[] getVector(String s) {
		s = s.toLowerCase();
		if(vectors == null)
			skipwiki();
		
		if(vectors.containsKey(s))
			return vectors.get(s);
		
		return vectors.get(unknown);
	}
}
