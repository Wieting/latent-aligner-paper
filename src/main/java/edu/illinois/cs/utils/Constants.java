package edu.illinois.cs.utils;

public class Constants {

	public static String rte5_train = "data/acl10/data/RTE5_MainTask_DevSet.xml";
	public static String rte5_test = "data/acl10/data/RTE5_MainTask_TestSet_Gold.xml";
	
	public static String msr_train = "data/MSR/msr_paraphrase_train.txt";
	public static String msr_test = "data/MSR/msr_paraphrase_test.txt";
	
	public static String s_msr_train = "data/MSR_small/msr_paraphrase_train.txt";
	public static String s_msr_test = "data/MSR_small/msr_paraphrase_test.txt";
	
	public static String semeval_dev = "data/semeval/SICK_trial.txt";
	public static String semeval_train = "data/semeval/SICK_train.txt";
	public static String semeval_test = "data/semeval/SICK_test_annotated.txt";
	
	public static String ppdb_lex = "../ppdb2/ppdb-1.0-xl-lexical.processed.txt";
	public static String ppdb_o2m = "../ppdb2/ppdb-1.0-s-o2m.processed.txt";
	public static String ppdb_phrase = "../ppdb2/ppdb-1.0-s-phrasal.processed.txt";
	
	public static String paragram = "../ppdb2/paragram_vectors.txt";
	public static String skipwiki = "../ppdb2/skipgram_vectors.txt";
	
	public static int max_phrase_length = 7; //overwritten
	
	public static String stopwords = "data/resources/english";
	public static String editDistanceCache = "../ppdb2/editdistancecache";
	public static String wnsimCache = "../ppdb2/word_output.txt";
	
	public static String wordnet = "data/resources/WordNet-3.0/dict";
	public static String univ_pos = "data/resources/en-ptb.map";
	
	public static int null_start = -1;
	public static int null_end = -1;
	public static String null_text = "__NULL__";
	public static String null_pos = "_NULL_POS_";
	public static String null_ner = "_NULL_NER_";
	
	public static String phrase_pairs = "../ppdb2/phrase_pairs";
	public static String oracle_parse = "../ppdb2/phrase_pairs.scores.parse_oracle.txt";
	public static String oracle_linear= "../ppdb2/phrase_pairs.scores.linear_oracle.txt";
	public static String test_parse= "../ppdb2/phrase_pairs.scores.parse_test.txt";
	public static String test_linear= "../ppdb2/phrase_pairs.scores.linear_test.txt";
}
