package edu.illinois.cs.utils;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

import edu.illinois.cs.cogcomp.core.io.LineIO;

public class ParagramUtils {

	public static HashMap<String,double[]> vectors = null;
	public static String unknown = "UUUNKKK";
	
	public static void loadParagram() {
		vectors = new HashMap<String,double[]>();
		
		int count =0;
		try {
			for (String line : LineIO.read(Constants.paragram)) {
				line = line.trim();
				if(line.length() > 0) {
					String[] arr = line.split("\\s");
					String word = arr[0];
					double[] vec = new double[25];
					for(int i=1; i < arr.length; i++) {
						vec[i-1] = Double.parseDouble(arr[i]);
					}
					vectors.put(word, vec);
					count++;
				}				
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static double paragramScore(String w1, String w2) {
		double[] d1 = getVector(w1);
		double[] d2 = getVector(w2);
		
		return cosine(d1,d2);
	}
	
	public static double[] add(ArrayList<double[]> lis) {
		double[] sum = new double[lis.get(0).length];
		for(int i=0; i < lis.size(); i++) {
			sum = add(sum,lis.get(i));
		}
		return sum;
	}
	
	public static double[] getVector(String s) {
		s = s.toLowerCase();
		if(vectors == null)
			loadParagram();
		
		if(vectors.containsKey(s)) {
			//System.out.println(s);
			//System.out.println(vectors.get(s));
			return vectors.get(s);
		}
		
		//System.out.println(vectors.get(unknown)+" "+s);
		return vectors.get(unknown);
	}
	
	public static double cosine(double[] vec1, double[] vec2) {
		double cosine = 0;
		double t1 = 0;
		double t2 = 0;
		
		for(int i=0; i < vec1.length; i++) {
			cosine += vec1[i]*vec2[i];
			t1 += vec1[i]*vec1[i];
			t2 += vec2[i]*vec2[i];
		}
		
		return cosine / (Math.sqrt(t1)*Math.sqrt(t2));
	}
	
	public static double[] add(double[] vec1, double[] vec2) {
		double[] sum = new double[25];
		
		for(int i=0; i < vec1.length; i++) {
			sum[i] = vec1[i] + vec2[i];
		}
		
		return sum;
	}
	
	public static void main(String[] args) {
		String dog = "dog";
		String cat = "cat";
		String puppy = "puppy";
		
		System.out.println(paragramScore(dog,cat)+" "+dog+" "+cat);
		System.out.println(paragramScore(dog,puppy)+" "+dog+" "+puppy);		
	}
	
}
