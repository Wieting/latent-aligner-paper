package edu.illinois.cs.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.apache.commons.math3.stat.correlation.SpearmansCorrelation;
import org.mapdb.DB;
import org.mapdb.DBMaker;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;
import edu.illinois.cs.features.Constituent;
import edu.illinois.cs.features.FeatureGenerator;
import edu.illinois.cs.features.FeatureGenerator1;
import edu.illinois.cs.features.PPDB;
import edu.illinois.cs.learning.LexManager;
import edu.illinois.cs.learning.Weight;
import edu.illinois.cs.learning.WeightVector;
import edu.illinois.cs.readers.Example;
import edu.stanford.nlp.util.Pair;

public class LearningUtils {

	public static void initWeights(WeightVector wv, LexManager lm, FeatureGenerator fea) {
		String featureName = "node-absWNSIM";
		lm.previewFeature(featureName);
		wv.setWElement(lm.getFeatureID(featureName), 1.0);

		//String[] arr = {"p_PPDBSIM","n_one-is-a-stop","p_WN-synonym","n_WN-antonym","p_WN-hypernym","n_WN-sibling",
		//		"p_both-nums-equal","n_both-nums-not-equal","p_PARAGRAMSIM","p_PPDB_3tt","p_PPDB_1tt","p_PPDB_3ft",
		//		"p_PPDB_1ft","p_PPDB_3ff","p_PPDB_1ff","p_PARAGRAM_3tt","p_PARAGRAM_1tt","p_PARAGRAM_3ft",
		//		"p_PARAGRAM_1ft","p_PARAGRAM_3ff","p_PARAGRAM_1ff","n_distortion"};

		HashSet<String> feas = new HashSet<String>();
		for(String s : fea.featurecache.keySet()) {
			Map<String,Double> t = fea.featurecache.get(s);
			for(String ss: t.keySet()) {
				if(ss.startsWith("p_") || ss.startsWith("n_"))
					feas.add(ss);
			}
		}

		ArrayList<String> features = new ArrayList<String>(feas);

		for(String s: features) {
			System.out.println(s);
			lm.previewFeature(s);
			if(s.startsWith("p_"))
				wv.setWElement(lm.getFeatureID(s), 1.0);
			if(s.startsWith("n_"))
				wv.setWElement(lm.getFeatureID(s), -1.0);
		}
	}

	public static FeatureGenerator getFG(ResourceManager rm, String words, String phrases) {
		FeatureGenerator fea = null;
		String featurename = rm.getString("featureset");
		

		if(featurename.equals("features1")) {
			System.out.println("Using FeatureGenerator1...");
			try {
			fea = new FeatureGenerator1(rm, words, phrases);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}


		return fea;
	}

	public static void writeWV(double[] w, LexManager lm) {

		ArrayList<Weight> weights = new ArrayList<Weight>();
		for(String s: lm.feaStr2Id_map.keySet()) {
			//System.out.println(s+" "+w[lm.feaStr2Id_map.get(s)]);
			weights.add(new Weight(s,w[lm.feaStr2Id_map.get(s)]));
		}

		Collections.sort(weights, new Comparator<Weight>(){
			public int compare(Weight w1, Weight w2) {
				//System.out.println("HERE");
				double diff = Math.abs(w1.d) - Math.abs(w2.d);
				diff = diff * 10000000;
				int d = (int) diff;
				//System.out.println(d);
				return d;
			}
		});

		for(Weight weight: weights) {
			System.out.println(weight.s+" "+weight.d);
		}
	}

	public static void addToMap(ArrayList<Example> data, HashMap<String,Map<String,Double>> cache, FeatureGenerator fea) {
		Constituent nullWord = new Constituent();
		nullWord.start = Constants.null_start;
		nullWord.end = Constants.null_end;
		nullWord.surfaceText = Constants.null_text;
		nullWord.pos = Constants.null_pos;
		nullWord.ner = Constants.null_ner;
		nullWord.lemma = Constants.null_text;

		for(int i=0; i < data.size(); i++) {
			Example e = data.get(i);
			ArrayList<Constituent> s_constituents = data.get(i).s_constituents;
			ArrayList<Constituent> l_constituents = data.get(i).l_constituents;

			for(int j=0; j < s_constituents.size()+1; j++) {
				for(int k=0; k < l_constituents.size()+1; k++) {
					if (j < s_constituents.size() ||  k < l_constituents.size()) {
						Constituent sw = null;
						Constituent lw = null;

						if (j < s_constituents.size())
							sw = s_constituents.get(j);
						else
							sw = nullWord;

						if (k < l_constituents.size())
							lw = l_constituents.get(k);
						else
							lw = nullWord;

						Map<String,Double> map = fea.getNodeAlignmentFeatures(e, lw, sw);
						String key = lw.id+"_"+sw.id;
						//System.out.println(key);
						cache.put(key, map);
					}
				}
			}
		}
	}

	public static Pair<Double, int[]> evaluate(List<Example> tePairList, double[] score) {
		assert score.length == tePairList.size();
		double[] gold = new double[tePairList.size()];
		double[] prediction = new double[score.length];

		int[] errors = new int[score.length];

		double tp = 0.0;
		double fp = 0.0;
		double fn = 0.0;
		double tn = 0.0;

		for (int i = 0; i < score.length; i++) {
			if (score[i] > 0)
				prediction[i] = 1;
			else
				prediction[i] = -1;

			Example tePair = tePairList.get(i);
			gold[i] = tePair.label;

			if (prediction[i] == 1 && gold[i] == 1) {
				tp += 1;
				errors[i] = 1;
			}
			else if (prediction[i] == -1 && gold[i] == 1) {
				fn += 1;
			}
			else if (prediction[i] == 1 && gold[i] == -1) {
				fp += 1;
			}
			else {
				tn += 1;
				errors[i] = 1;
			}

		}

		double acc = (tp + tn) / (tp + tn + fp + fn) * 100;
		//System.out.println("There are " + (tp + fn) + " positive examples. Among them, " + tp + " are correct.");
		//System.out.println("There are " + (tn + fp) + " negative examples. Among them, " + tn + " are correct.");
		//System.out.println("ACC is : " + StringUtils.getFormattedTwoDecimal(acc));

		return new Pair<Double, int[]>(acc,errors);
	}

	public static Pair<Double, int[]> evaluateEntailment(List<Example> tePairList, double[] score) {
		assert score.length == tePairList.size();
		String[] gold = new String[tePairList.size()];
		double[] prediction = new double[score.length];

		int[] errors = new int[score.length];

		double tp = 0.0;
		double fp = 0.0;
		double fn = 0.0;
		double tn = 0.0;

		for (int i = 0; i < score.length; i++) {
			int label = 0;
			if(Math.abs(2-score[i]) <= Math.abs(1-score[i]) && Math.abs(2-score[i]) <= Math.abs(0-score[i]))
				label = 2;
			if(Math.abs(1-score[i]) <= Math.abs(0-score[i]) && Math.abs(1-score[i]) <= Math.abs(2-score[i]))
				label = 1;
			if(Math.abs(0-score[i]) <= Math.abs(1-score[i]) && Math.abs(0-score[i]) <= Math.abs(2-score[i]))
				label = 0;

			prediction[i] = label;

			Example tePair = tePairList.get(i);
			gold[i] = tePair.s_label;

			if (prediction[i] == 0 && gold[i].equals("CONTRADICTION")) {
				tp += 1;
				errors[i] = 1;
			}
			else if (prediction[i] == 1 && gold[i].equals("NEUTRAL")) {
				tp += 1;
				errors[i] = 1;
			}
			else if (prediction[i] == 2 && gold[i].equals("ENTAILMENT")) {
				tp += 1;
				errors[i] = 1;
			}
			else {
				fp += 1;
			}

		}

		double acc = (tp + tn) / (tp + tn + fp + fn) * 100;
		//System.out.println("ACC is : " + StringUtils.getFormattedTwoDecimal(acc));

		return new Pair<Double, int[]>(acc,errors);
	}


	public static void prettyPrintFeatures(Example e, FeatureGenerator fea) {
		Constituent nullWord = new Constituent();

		nullWord.start = Constants.null_start;
		nullWord.end = Constants.null_end;
		nullWord.surfaceText = Constants.null_text;
		nullWord.pos = Constants.null_pos;
		nullWord.ner = Constants.null_ner;
		nullWord.lemma = Constants.null_text;

		List<Constituent> textNodes = e.l_constituents;
		List<Constituent> hypNodes = e.s_constituents;

		for (int i=0; i < hypNodes.size()+1; i++) {
			for (int j=0; j < textNodes.size()+1; j++) {

				Constituent sw = null;
				Constituent lw = null;

				if (i < hypNodes.size())
					sw = hypNodes.get(i);
				else
					sw = nullWord;

				if (j < textNodes.size())
					lw = textNodes.get(j);
				else
					lw = nullWord;

				//do not allow null to null
				if (i < hypNodes.size() || j < textNodes.size()) {
					Map<String, Double> map = fea.getNodeAlignmentFeatures(e,lw,sw);
					String hypoutput = "";
					for(int ii=i-3; ii <= i+3; ii++) {
						if(ii >= 0 && ii < hypNodes.size()) {
							if(ii==i)
								hypoutput += ">>"+hypNodes.get(ii).surfaceText+"<< ";
							else
								hypoutput += hypNodes.get(ii).surfaceText+" ";
						}
					}

					System.out.println(hypoutput);

					String textoutput = "";
					for(int jj=j-3; jj <= j+3; jj++) {
						if(jj >= 0 && jj < textNodes.size()) {
							if(jj==j)
								textoutput += ">>"+textNodes.get(jj).surfaceText+"<< ";
							else
								textoutput += textNodes.get(jj).surfaceText+" ";
						}
					}
					System.out.println(textoutput);
					for(String ss: map.keySet()) {
						System.out.println("\t"+ss+":"+map.get(ss));
					}
				}
			}
		}
	}

	public static Pair<double[], int[]> evaluateSimilarity(
			List<Example> tePairList, double[] score) {

		assert score.length == tePairList.size();
		double[] arr = new double[3];
		double[] gold = new double[tePairList.size()];


		for (int i = 0; i < score.length; i++) {
			Example tePair = tePairList.get(i);
			gold[i] = tePair.simscore;
		}

		arr[0] = new SpearmansCorrelation().correlation(gold, score);
		arr[1] = new PearsonsCorrelation().correlation(gold, score);
		arr[2] = getMSE(gold, score);
		
		return new Pair<double[], int[]>(arr,null);
	}
	
	public static double getMSE(double[] gold, double[] score) {
		double total_error = 0.;
		
		for(int i=0; i < gold.length; i++) {
			double error = gold[i] - score[i];
			error = error*error;
			total_error += error;
			//System.out.println(gold[i]+" "+score[i]);
		}
		
		return total_error / gold.length;
	}
}
