package edu.illinois.cs.utils;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import edu.mit.jwi.IRAMDictionary;
import edu.mit.jwi.RAMDictionary;
import edu.mit.jwi.data.ILoadPolicy;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.IPointer;
import edu.mit.jwi.item.ISynset;
import edu.mit.jwi.item.ISynsetID;
import edu.mit.jwi.item.IWord;
import edu.mit.jwi.item.IWordID;
import edu.mit.jwi.item.POS;
import edu.mit.jwi.item.Pointer;
import edu.stanford.nlp.util.Pair;

public class WNUtils {

	public static IRAMDictionary dict = null;

	public static void loadWN( String wnhome ) {
		URL url;
		try {
			url = new URL ( "file" , null , wnhome );
			dict = new RAMDictionary (url, ILoadPolicy.NO_LOAD);
			dict.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean isSynonym(String lemma1, String pos1, String lemma2, String pos2) {
		if(dict == null)
			loadWN(Constants.wordnet);

		ArrayList<ISynset> lis1 = getAllSynsets(lemma1,pos1);
		ArrayList<ISynset> lis2 = getAllSynsets(lemma2,pos2);

		for(ISynset is1: lis1) {
			for(ISynset is2: lis2) {
				if(is1.equals(is2))
					return true;
			}
		}

		return false;
	}

	public static boolean isAntonym(String lemma1, String pos1, String lemma2, String pos2) {
		if(dict == null)
			loadWN(Constants.wordnet);

		ArrayList<IWord> ants = getAntonyms(lemma1,pos1);
		ArrayList<IWord> wds = getIWords(lemma2,pos2);

		for(IWord ant: ants) {
			for(IWord iw: wds) {
				if(ant.equals(iw))
					return true;
			}
		}

		return false;
	}

	/*
	 * is lemma1 a hypernym of lemma2?
	 */
	public static boolean isHypernym(String lemma1, String pos1, String lemma2, String pos2) {
		if(dict == null)
			loadWN(Constants.wordnet);

		ArrayList<ISynset> lis1 = getAllSynsets(lemma1,pos1);
		ArrayList<ISynset> lis2 = getAllSynsets(lemma2,pos2);

		ArrayList<Pair<ISynset,Integer>> hypers2 = new ArrayList<Pair<ISynset,Integer>>();
		for(int i=0; i < lis2.size(); i++) {
			hypers2 = getParents(lis2.get(i),0,hypers2);
		}
		
		for(int i=0; i < lis1.size(); i++) {
			ISynset is1 = lis1.get(i);
			for(int j=0; j < hypers2.size(); j++) {
				ISynset is2 = hypers2.get(j).first;
				int count = hypers2.get(j).second;
				if(count == 0)
					continue;
				if(is2.equals(is1))
					return true;
			}
		}
		
		return false;
	}

	/*
	 * are lemma1 and lemma2 siblings?
	 */
	public static boolean isSibling(String lemma1, String pos1, String lemma2, String pos2, int maxDepth) {
		if(dict == null)
			loadWN(Constants.wordnet);

		ArrayList<ISynset> lis1 = getAllSynsets(lemma1,pos1);
		ArrayList<ISynset> lis2 = getAllSynsets(lemma2,pos2);

		//System.out.println(lis1.size());
		//System.out.println(lis2.size());
		ArrayList<Pair<ISynset,Integer>> hypers1 = new ArrayList<Pair<ISynset,Integer>>();
		for(int i=0; i < lis1.size(); i++) {
			hypers1 = getParents(lis1.get(i),0,hypers1);
		}
		
		ArrayList<Pair<ISynset,Integer>> hypers2 = new ArrayList<Pair<ISynset,Integer>>();
		for(int i=0; i < lis2.size(); i++) {
			hypers2 = getParents(lis2.get(i),0,hypers2);
		}
		
		//check if any hypers are same, depth > 0, one has depth <= maxDepth
		for(int i=0; i<hypers1.size(); i++) {
			ISynset is1 = hypers1.get(i).first;
			int count1 = hypers1.get(i).second;
			if(count1 == 0)
				continue;
			for(int j=0; j < hypers2.size(); j++) {
				ISynset is2 = hypers2.get(j).first;
				int count2 = hypers2.get(j).second;
				if(count2 == 0)
					continue;
				if(is2.equals(is1)) {
					//System.out.println(is1);
					if(count1 <= maxDepth || count2 <= maxDepth)
					return true;
				}
			}
		}
		
		return false;
	}

	public static void main(String[] args) {
		String w_dog = "dog";
		String w_cad = "cad";
		String w_canine = "canine";
		String w_apple = "apple";
		String w_orange = "orange";
		String w_lamp = "lamp";
		String noun = "NN";

		String w_man = "man";
		String w_woman = "woman";
		
		System.out.println(isSynonym(w_dog,noun,w_cad,noun)+" "+w_dog+" "+w_cad);
		System.out.println(isAntonym(w_man,noun,w_woman,noun)+" "+w_man+" "+w_woman);
		System.out.println(isHypernym(w_canine,noun,w_dog,noun)+" "+w_canine+" "+w_dog);
		System.out.println(isHypernym(w_dog,noun,w_canine,noun)+" "+w_dog+" "+w_canine);
		System.out.println(isHypernym(w_dog,noun,w_cad,noun)+" "+w_dog+" "+w_cad);
		System.out.println(isSibling(w_canine,noun,w_dog,noun,2)+" "+w_canine+" "+w_dog);
		System.out.println(isSibling(w_apple,noun,w_orange,noun,2)+" "+w_apple+" "+w_orange);
		System.out.println(isSibling(w_apple,noun,w_dog,noun,2)+" "+w_apple+" "+w_dog);
		System.out.println(isSibling(w_lamp,noun,w_apple,noun,2)+" "+w_lamp+" "+w_apple);
		System.out.println(isSibling("a","CC","yard","NN",2)+" a yard");
	}

	public static ArrayList<IWord> getIWords(String st, String pos) {
		ArrayList<IWord> iwds = new ArrayList<IWord>();
		if(getPOS(pos)==null)
			return iwds;
		
		IIndexWord idxWord = dict.getIndexWord(st, getPOS(pos));

		if(idxWord == null)
			return iwds;

		List<IWordID> senses = idxWord.getWordIDs(); // 1 st meaning
		for(IWordID iw : senses) {
			//System.out.println(iw);
			iwds.add(dict.getWord(iw));
		}

		return iwds;
	}

	public static ArrayList<ISynset> getAllSynsets(String word, String pos) {
		ArrayList<ISynset> syns = new ArrayList<ISynset>();
		if(getPOS(pos)==null)
			return syns;
		
		IIndexWord idxWord = dict.getIndexWord(word, getPOS(pos)) ;
		if(idxWord==null)
			return syns;
		
		List<IWordID> senses = idxWord.getWordIDs();
		for(IWordID iw : senses) {
			ISynset is = dict.getSynset(iw.getSynsetID());
			List<IWord> words = is.getWords();
			syns.add(is);
		}

		return syns;
	}

	public static ArrayList<IWord> getAntonyms(String st, String p) {
		ArrayList<IWord> iwds = getIWords(st,p);
		ArrayList<IWord> antonyms = new ArrayList<IWord>();
		for(IWord iw: iwds) {
			List<IWordID> ants = iw.getRelatedMap().get(Pointer.ANTONYM);
			if(ants==null)
				continue;
			for(IWordID id: ants) {
				//System.out.println(dict.getWord(id));
				antonyms.add(dict.getWord(id));
			}
		}

		return antonyms;
	}

	public static ArrayList<Pair<ISynset,Integer>> getParents(ISynset is, int curr, ArrayList<Pair<ISynset,Integer>> lis) {

		if(curr == 15) {
			return lis;
		}

		Pair<ISynset,Integer> p = new Pair<ISynset,Integer>(is,curr);
		lis.add(p);

		List<ISynsetID> sids = is.getRelatedSynsets(Pointer.HYPERNYM);
		curr++;
		for(ISynsetID sid : sids) {
			getParents(dict.getSynset(sid), curr, lis);
		}
			
		return lis;
	}
	
	
	
	public static POS getPOS(String pos) {

		pos = Utils.getUnivPOS(pos);

		if(pos.equalsIgnoreCase("VERB"))
			return POS.VERB;
		else if(pos.startsWith("ADJ"))
			return POS.ADJECTIVE;
		else if(pos.startsWith("ADV"))
			return POS.ADVERB;
		else if(pos.startsWith("NOUN"))
			return POS.NOUN;

		return null; //just default
	}
}
