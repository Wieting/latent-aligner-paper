package edu.illinois.cs.features;

import java.util.Vector;

public class EditDistance {
	
	enum OPERATORS
	{
		NONE, INS, DEL, REP
	};
	
	public static double normalizedEditDistance(String text, String hyp)
	{
		text = text.toLowerCase();
		hyp = hyp.toLowerCase();
		
		// except the dp_path[src.length()][trg.length()]
		double dp_table[][] = null;
		OPERATORS dp_path[][] = null;

		int n = text.length();
		int m = hyp.length();

		dp_table = new double[text.length() + 1][];
		dp_path = new OPERATORS[text.length() + 1][];

		for (int i = 0; i <= n; i++)
		{
			dp_table[i] = new double[hyp.length() + 1];
			dp_path[i] = new OPERATORS[hyp.length() + 1];
			for (int j = 0; j <= m; j++)
				dp_path[i][j] = OPERATORS.NONE;
		}

		// printDPTable();
		for (int i = n; i >= 0; i--)
		{
			for (int j = m; j >= 0; j--)
			{

				// starting point
				if (i == n && j == m)
				{
					dp_table[i][j] = 0;
					continue;
				}

				// else, do dynamic programming
				Vector<OPERATORS> allow_operators = new Vector<OPERATORS>();

				if (i == n)
				{
					allow_operators.add(OPERATORS.INS);
				}
				else if (j == m)
				{
					allow_operators.add(OPERATORS.DEL);
				}
				else
				{
					allow_operators.add(OPERATORS.INS);
					allow_operators.add(OPERATORS.DEL);
					allow_operators.add(OPERATORS.REP);
				}

				double ins_cost, del_cost, rep_cost;
				ins_cost = del_cost = rep_cost = Double.NEGATIVE_INFINITY;

				for (OPERATORS op : allow_operators)
				{

					if (op == OPERATORS.INS)
					{
						ins_cost = -1.0 + dp_table[i][j + 1];
					}

					if (op == OPERATORS.DEL)
					{
						del_cost = -1.0 + dp_table[i + 1][j];
					}

					if (op == OPERATORS.REP)
					{
						// System.out.println(src.charAt(i) + "#" +
						// trg.charAt(j)) ;
						double r = 0;
						if (text.charAt(i) == hyp.charAt(j))
							r = 0;
						else if ((text.charAt(i) + "").equalsIgnoreCase(""
								+ hyp.charAt(j)))
							r = -0.5;
						else
							r = -1;

						rep_cost = r + dp_table[i + 1][j + 1];
					}
				}

				// ugly code: fill out the best move and
				if (ins_cost >= del_cost && ins_cost >= rep_cost)
				{
					dp_table[i][j] = ins_cost;
					dp_path[i][j] = OPERATORS.INS;
				}
				else if (del_cost >= ins_cost && del_cost >= rep_cost)
				{
					dp_table[i][j] = del_cost;
					dp_path[i][j] = OPERATORS.DEL;
				}
				else
				{
					dp_table[i][j] = rep_cost;
					dp_path[i][j] = OPERATORS.REP;
				}

			}
		}

		Vector<String> res = new Vector<String>();
		int cur_i = 0, cur_j = 0;

		while (cur_i != text.length() || cur_j != hyp.length())
		{

			if (dp_path[cur_i][cur_j] == OPERATORS.INS)
			{
				res.add("*#" + hyp.charAt(cur_j));
				cur_j += 1;
			}
			else if (dp_path[cur_i][cur_j] == OPERATORS.DEL)
			{
				res.add(text.charAt(cur_i) + "#*");
				cur_i += 1;
			}
			else
			{// rep
				res.add(text.charAt(cur_i) + "#" + hyp.charAt(cur_j));
				cur_i += 1;
				cur_j += 1;
			}
			// System.out.println(res.get(res.size()-1));
		}

		return 1.0 + (dp_table[0][0] / res.size());
	}

}
