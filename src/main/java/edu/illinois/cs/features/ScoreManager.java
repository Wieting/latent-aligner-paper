package edu.illinois.cs.features;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.utils.Constants;

public class ScoreManager {

	HashMap<String, Double> editdistance = new HashMap<String, Double>();
	String editdistancecachefile = Constants.editDistanceCache;

	HashMap<String, Double> wnsim = new HashMap<String,Double>();
	String wnsimcachefile = Constants.wnsimCache;

	public ScoreManager() {
		loadEditCache(editdistancecachefile);
		loadWNSimCache(wnsimcachefile);
	}

	private void loadWNSimCache(String cachefile) {
		File f = new File(cachefile);

		if(!f.exists())
			return;

		try {
			ArrayList<String> lines = LineIO.read(cachefile);
			for(String s: lines) {
				String[] arr = s.split("\\s");
				String k = arr[0]+"@@@"+arr[1];
				Double d = Double.parseDouble(arr[2]);
				wnsim.put(k, d);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void loadEditCache(String cachefile) {

		File f = new File(cachefile);

		if(!f.exists())
			return;

		try {
			ArrayList<String> lines = LineIO.read(cachefile);
			for(String s: lines) {
				String[] arr = s.split("@@@");
				String k = arr[0]+"@@@"+arr[1];
				Double d = Double.parseDouble(arr[2]);
				wnsim.put(k, d);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public double getWNSim(String w1, String w2) {
		String key1 = w1+"@@@"+w2;
		String key2 = w2+"@@@"+w1;
		if(wnsim.containsKey(key1)) {
			return wnsim.get(key1);
		}
		if(wnsim.containsKey(key2)) {
			return wnsim.get(key2);
		}
		
		if(w1.equals(Constants.null_text) || w2.equals(Constants.null_text))
			return 0.;
		
		if(w1.toLowerCase().equals(w2.toLowerCase()))
			return 1.;
		
		//System.out.println("Error: "+w1+" "+w2+" not in WNSim cache");
		return 0.;
	}
	
	public double getEditDistance(String w1, String w2) {
		w1 = w1.toLowerCase();
		w2 = w2.toLowerCase();
		String key1 = w1+"@@@"+w2;
		String key2 = w2+"@@@"+w1;
		if(editdistance.containsKey(key1))
			return editdistance.get(key1);

		double d = EditDistance.normalizedEditDistance(w1, w2);
		editdistance.put(key1,d);
		editdistance.put(key2, d);
		return d;
	}

	public void closecaches() {
		ArrayList<String> lines = new ArrayList<String>();
		for(String s: editdistance.keySet()) {
			String l = s+"@@@"+editdistance.get(s);
			lines.add(l);
		}

		/*try {
			LineIO.write(editdistancecachefile, lines);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}

}
