package edu.illinois.cs.features;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.readers.Example;
import edu.illinois.cs.utils.Constants;
import edu.illinois.cs.utils.Utils;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.Pair;

public class PPDB {

	public HashMap<String, HashSet<String>> lex_map;
	public HashMap<String, HashSet<String>> o2m_map;
	public HashMap<String, HashSet<String>> phrase_map;

	public PPDB(StanfordCoreNLP pipeline) {
		lex_map = Utils.loadPPDB(Constants.ppdb_lex, pipeline);
		o2m_map = Utils.loadPPDB(Constants.ppdb_o2m, pipeline);
		phrase_map = Utils.loadPPDB(Constants.ppdb_phrase, pipeline);
	}
	
	public PPDB(ResourceManager rm, StanfordCoreNLP pipeline) {
		lex_map = Utils.loadPPDB(Constants.ppdb_lex, pipeline);
		phrase_map = Utils.loadPPDB(rm.getString("ppdb_phrase"), pipeline);
	}

	public PPDB(String lex, String o2m, String phr, StanfordCoreNLP pipeline) {
		lex_map = Utils.loadPPDB(lex, pipeline);
		phrase_map = Utils.loadPPDB(phr, pipeline);
	}
	
	public double getPPDBScore(Constituent textConstituent, Constituent hypothesisConstituent) {
		String text_w = textConstituent.surfaceText.toLowerCase();
		String hyp_w = hypothesisConstituent.surfaceText.toLowerCase();
		if(text_w.equals(hyp_w))
			return 1.;
		if(!lex_map.containsKey(textConstituent))
			return 0.;
		HashSet<String> temp = lex_map.get(textConstituent);
		if(temp.contains(hypothesisConstituent))
			return 1.;
		return 0.;
	}

	public PPDBcontainer analyzeSentencePair(Example e) {
		PPDBcontainer c = new PPDBcontainer();
		getLexical(c,e);
		getPhrasal(c,e);
		return c;
	}

	private void getPhrasal(PPDBcontainer c, Example e) {
		List<CoreLabel> l_toks = e.l_map.get(TokensAnnotation.class);
		List<CoreLabel> s_toks = e.s_map.get(TokensAnnotation.class);

		for(int i=0; i < l_toks.size(); i++) {
			for(int j=0; j < s_toks.size(); j++) {
				checkForMatch(i,l_toks,j,s_toks,c);
			}	
		}
	}

	private void checkForMatch(int i, List<CoreLabel> l_toks, int j,
			List<CoreLabel> s_toks, PPDBcontainer c) {
		
		String l_phrase = "";
		String l_phrase_norm="";
		for(int l_i=i; l_i < Constants.max_phrase_length+i; l_i++) {
			if(l_i >= l_toks.size())
				continue;
			
			//get phrase candidate
			l_phrase = l_phrase+" "+l_toks.get(l_i).originalText();
			l_phrase_norm = l_phrase_norm+" "+l_toks.get(l_i).originalText().toLowerCase();
			//System.out.println(l_phrase_norm+" "+l_phrase_norm.trim().equals("by altering the")+" "+phrase_map.containsKey("by altering the"));
			l_phrase_norm = l_phrase_norm.trim();
			if(!phrase_map.containsKey(l_phrase_norm))
				continue;
			HashSet<String> temp = phrase_map.get(l_phrase_norm);
			String s_phrase="";
			String s_phrase_norm="";
			for(int s_j=j; s_j < Constants.max_phrase_length+j; s_j++) {
				if(s_j >= s_toks.size())
					continue;
				
				//get phrase candidate
				s_phrase = s_phrase +" "+s_toks.get(s_j).originalText();
				s_phrase_norm = s_phrase_norm +" "+s_toks.get(s_j).originalText().toLowerCase();
				s_phrase_norm = s_phrase_norm.trim();
				
				//compare
				if(temp.contains(s_phrase_norm)) {
					Constituent con1 = new Constituent();
					con1.start=i;
					con1.end=l_i;
					con1.surfaceText=l_phrase.trim();

					Constituent con2 = new Constituent();
					con2.start=j;
					con2.end=s_j;
					con2.surfaceText=s_phrase.trim();

					c.phrmap.add(new Pair<Constituent, Constituent>(con1,con2));
				}
			}
		}
	}

	private void getLexical(PPDBcontainer c, Example e) {
		List<CoreLabel> l_toks = e.l_map.get(TokensAnnotation.class);
		List<CoreLabel> s_toks = e.s_map.get(TokensAnnotation.class);

		for(int i=0; i < l_toks.size(); i++) {
			String l_tok = l_toks.get(i).originalText();
			String l_tok_norm = l_tok.toLowerCase();
			if(!lex_map.containsKey(l_tok_norm))
				continue;
			HashSet<String> temp = lex_map.get(l_tok_norm);
			for(int j=0; j < s_toks.size(); j++) {
				String s_tok = s_toks.get(j).originalText();
				String s_tok_norm = s_tok.toLowerCase();
				if(temp.contains(s_tok_norm)) {
					Constituent con1 = new Constituent();
					con1.start=i;
					con1.end=i;
					con1.surfaceText=l_tok;

					Constituent con2 = new Constituent();
					con2.start=j;
					con2.end=j;
					con2.surfaceText=s_tok;

					c.lexmap.add(new Pair<Constituent, Constituent>(con1,con2));
				}	
			}
		}
	}
}
