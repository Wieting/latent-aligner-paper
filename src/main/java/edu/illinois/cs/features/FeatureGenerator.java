package edu.illinois.cs.features;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.readers.Example;
import edu.illinois.cs.utils.Constants;

public abstract class FeatureGenerator {

	public PPDB ppdb;
	public ScoreManager scoremanager;
	public ResourceManager rm = null;

	public HashMap<String,Map<String,Double>> featurecache = null;
	
	public FeatureGenerator() {
		scoremanager = new ScoreManager();
	}

	public FeatureGenerator(ResourceManager rm) {
		this.rm = rm;
		scoremanager = new ScoreManager();

		if(rm.containsKey("cachefile") && rm.getBoolean("loadfeaturecache"))
			setFeatureCacheMapDB();
	}

	public FeatureGenerator(ResourceManager rm, PPDB ppdb) {
		this.rm = rm;
		this.ppdb = ppdb;
		scoremanager = new ScoreManager();

		if(rm.containsKey("cachefile") && rm.getBoolean("loadfeaturecache"))
			setFeatureCacheMapDB();
	}

	public void setFeatureCacheMapDB() {
		
		System.out.println("Loading features from mapdb...");
		featurecache = new HashMap<String,Map<String,Double>>();
	}
	
	public void closeCache() {
		scoremanager.closecaches();
	}

	abstract public Map<String, Double> getNodeAlignmentFeatures(Example e,
			Constituent lw, Constituent sw);
}
