package edu.illinois.cs.features;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import edu.illinois.cs.readers.Example;
import edu.illinois.cs.utils.Constants;
import edu.illinois.cs.utils.Utils;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.util.Pair;

public class Ngrams {

	public ArrayList<Pair<Constituent,Constituent>> onegram_map = new ArrayList<Pair<Constituent,Constituent>>();
	public ArrayList<Pair<Constituent,Constituent>> twogram_map = new ArrayList<Pair<Constituent,Constituent>>();
	public ArrayList<Pair<Constituent,Constituent>> threegram_map = new ArrayList<Pair<Constituent,Constituent>>();
	public ArrayList<Pair<Constituent,Constituent>> fourgram_map = new ArrayList<Pair<Constituent,Constituent>>();
	public ArrayList<Pair<Constituent,Constituent>> fivegram_map = new ArrayList<Pair<Constituent,Constituent>>();

	HashSet<String> stopwords;

	public Ngrams(HashSet<String> stopwords) {
		this.stopwords = stopwords;
	}

	public void analyzeSentencePair(Example e) {
		getNgram(e, onegram_map, 1);
		getNgram(e, twogram_map, 2);
		getNgram(e, threegram_map, 3);
		getNgram(e, fourgram_map, 4);
		getNgram(e, fivegram_map, 5);
	}

	private void getNgram(Example e, ArrayList<Pair<Constituent,Constituent>> map, int size) {
		List<CoreLabel> l_toks = e.l_map.get(TokensAnnotation.class);
		List<CoreLabel> s_toks = e.s_map.get(TokensAnnotation.class);

		for (int j=0; j < s_toks.size(); j++) {
			CoreLabel st = s_toks.get(j);
			for(int k=0; k < l_toks.size(); k++) {
				CoreLabel lt = l_toks.get(k);
				//start an ngram
				if(st.originalText().equals(lt.originalText())) {
					getNgram(s_toks, j, l_toks, k, map,size);
				}
			}
		}
	}

	private void getNgram(List<CoreLabel> s_toks, int si,
			List<CoreLabel> l_toks, int li, ArrayList<Pair<Constituent,Constituent>> map, int maxsize) {

		int n = 0;
		boolean nonstop = false;
		//loop over long
		for(int i=li; i < l_toks.size(); i++) {
			if(si >= s_toks.size()) { // end of short phrase
				if(nonstop)
					check_size(si-1,i-1,n,s_toks,l_toks,map,maxsize);
				break;
			}

			CoreLabel s_tok = s_toks.get(si);
			CoreLabel l_tok = l_toks.get(i);
			String s_st = s_tok.originalText();  //short tok
			String l_st = l_tok.originalText();  //long tok

			if(s_st.equals(l_st)) {
				n++;
				if(!stopwords.contains(s_st)) {
					//System.out.println(s_st+" "+stopwords.size());
					nonstop = true;
				}
			}
			else {
				if(nonstop)
					check_size(si-1,i-1,n,s_toks,l_toks,map,maxsize);
				break;
			}
			
			if(i==l_toks.size() - 1) {
				if(nonstop)
					check_size(si,i,n,s_toks,l_toks,map,maxsize);
				break;
			}
			
			si++;
		}
		
		
		
		return;
	}

	private void check_size(int si, int i, int n, List<CoreLabel> s_toks,
			List<CoreLabel> l_toks, ArrayList<Pair<Constituent, Constituent>> map, int maxsize) {

		boolean ok = false;
		if(n==maxsize || (maxsize == 5 && n > 5))
			ok = true;
		if(!ok)
			return;
		
		Constituent con1 = new Constituent();
		con1.start=si - n + 1;
		con1.end=si;
		con1.surfaceText= Utils.getText(s_toks,con1.start, con1.end);

		Constituent con2 = new Constituent();
		con2.start= i - n + 1;
		con2.end=i;
		con2.surfaceText= Utils.getText(l_toks,con2.start, con2.end);

		map.add(new Pair<Constituent, Constituent>(con1,con2));
	}
}
