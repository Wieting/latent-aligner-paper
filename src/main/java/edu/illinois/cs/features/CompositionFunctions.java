package edu.illinois.cs.features;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.readers.Example;
import edu.illinois.cs.utils.Constants;
import edu.illinois.cs.utils.ParagramUtils;
import edu.illinois.cs.utils.SkipUtils;
import edu.illinois.cs.utils.Utils;

public class CompositionFunctions {

	public static HashMap<String,Integer> phrase_map;
	public static HashMap<Integer,Double> oracle_linear;
	public static HashMap<Integer,Double> oracle_parse;
	public static HashMap<Integer,Double> test_linear;
	public static HashMap<Integer,Double> test_parse;

	public static double[] CompositionModel(Example e, Constituent tc, Constituent hc, int offset) {
		double[] temp = new double[4];

		ArrayList<Constituent> textCons = e.l_constituents;
		ArrayList<Constituent> hypCons = e.s_constituents;

		int ti = tc.start;
		int hi = hc.start;

		if(ti==textCons.size() || hi == hypCons.size() || ti < 0 || hi < 0)
			return temp;

		String text_w = "";
		for(int i=ti-offset; i <= ti + offset; i++) {
			if( i >= 0 && i < textCons.size()) {
				String text = textCons.get(i).surfaceText;
				text_w += text+" ";
			}
		}
		text_w = text_w.trim();

		String hyp_w = "";
		for(int j=hi - offset; j <= hi + offset; j ++) {
			if( j >= 0 && j < hypCons.size()) {
				String text = hypCons.get(j).surfaceText;
				hyp_w += text+" ";
			}
		}
		hyp_w = hyp_w.trim();

		String key = "";
		String s1 = text_w.toLowerCase();
		String s2 = hyp_w.toLowerCase();
		if(s1.compareTo(s2) < 0) {
			key = s1+"|||"+s2+"|||1.0";
			if(phrase_map!=null && !phrase_map.containsKey(key))
				key = s2+"|||"+s1+"|||1.0";
		}
		else {
			key = s2+"|||"+s1+"|||1.0";
			if(phrase_map!=null && !phrase_map.containsKey(key))
				key = s1+"|||"+s2+"|||1.0";
		}
		
		//oracle parse
		if(oracle_parse == null) {
			oracle_parse = load_composition_map(oracle_parse, Constants.oracle_parse);
		}
		//System.out.println(key);
		//System.out.println(phrase_map.get(key));
		//System.out.println(oracle_parse.get(phrase_map.get(key)));
		
		temp[0] = (double) oracle_parse.get(phrase_map.get(key));

		//oracle linear
		if(oracle_linear == null) {
			oracle_linear = load_composition_map(oracle_linear, Constants.oracle_linear);
		}
		temp[1] = (double) oracle_linear.get(phrase_map.get(key));

		//test parse
		if(test_parse == null) {
			test_parse = load_composition_map(test_parse, Constants.test_parse);
		}
		temp[2] = (double) test_parse.get(phrase_map.get(key));

		//test linear
		if(test_linear == null) {
			test_linear = load_composition_map(test_linear, Constants.test_linear);
		}
		temp[3] = (double) test_linear.get(phrase_map.get(key));

		return temp;
	}
	
	private static void load_phrase_map() {
		try {
			phrase_map = new HashMap<String,Integer>();
			ArrayList<String> phrase_pairs = LineIO.read(Constants.phrase_pairs);
			
			for(int i=0; i < phrase_pairs.size(); i++) {
				phrase_map.put(phrase_pairs.get(i).trim(), (Integer) i);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static HashMap<Integer, Double> load_composition_map(
			HashMap<Integer, Double> map, String fname) {
		
		map = new HashMap<Integer,Double>();
		
		if(phrase_map == null)
			load_phrase_map();

		try{ 
			ArrayList<String> phrase_scores = LineIO.read(fname);

			for(int i=0; i < phrase_scores.size(); i++) {
				double score = Double.parseDouble(phrase_scores.get(i));
				map.put(i, score);
			}

		} catch(FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return map;
	}

	public static double LLMScore(int type, PPDB ppdb, Example e, Constituent tc, Constituent hc, int offset, boolean excludestops) {
		ArrayList<Constituent> textCons = e.l_constituents;
		ArrayList<Constituent> hypCons = e.s_constituents;

		int ti = tc.start;
		int hi = hc.start;

		if(ti==textCons.size() || hi == hypCons.size() || ti < 0 || hi < 0)
			return 0.;

		String word_1 = textCons.get(ti).surfaceText;
		String word_2 = hypCons.get(hi).surfaceText;

		ArrayList<double[]> vectors1 = new ArrayList<double[]>();

		for(int i=ti-offset; i <= ti + offset; i++) {
			if( i >= 0 && i < textCons.size() && i != ti) {
				String text = textCons.get(i).surfaceText;
				if(excludestops && (Utils.isStop(text)))
					continue;
				if(type == 0)
					vectors1.add(ParagramUtils.getVector(text));
				else if(type == 1)
					vectors1.add(SkipUtils.getVector(text));
			}
		}

		ArrayList<double[]> vectors2 = new ArrayList<double[]>();
		for(int j=hi - offset; j <= hi + offset; j ++) {
			if( j >= 0 && j < hypCons.size() && j != hi) {
				String text = hypCons.get(j).surfaceText;
				if(excludestops && (Utils.isStop(text)))
					continue;
				if(type == 0)
					vectors2.add(ParagramUtils.getVector(text));
				else if(type==1)
					vectors2.add(SkipUtils.getVector(text));
			}
		}

		ArrayList<double[]> small = vectors1.size() < vectors2.size() ? vectors1 : vectors2;
		ArrayList<double[]> big =  vectors2.size() > vectors1.size() ? vectors2 : vectors1;

		double total = 0;
		double count = 0;
		for(int i=0; i < small.size();i++) {
			double[] v = small.get(i);
			double max = 0;
			for(int j=0; j < big.size(); j++) {
				double[] t = big.get(j);
				double score = ParagramUtils.cosine(v,t);
				if(score > max)
					max = score;
			}
			total += max;
			count += 1;
		}

		if(count==0)
			return 0.;
		
		return total / count;
	}

	public static double AddScore(int type, PPDB ppdb, Example e, Constituent tc, Constituent hc, int offset) {
		ArrayList<Constituent> textCons = e.l_constituents;
		ArrayList<Constituent> hypCons = e.s_constituents;

		int ti = tc.start;
		int hi = hc.start;

		if(ti==textCons.size() || hi == hypCons.size() || ti < 0 || hi < 0)
			return 0.;
		
		ArrayList<double[]> vectors = new ArrayList<double[]>();

		for(int i=ti-offset; i <= ti + offset; i++) {
			if( i >= 0 && i < textCons.size()) {
				String text = textCons.get(i).surfaceText;
				if(type == 0)
					vectors.add(ParagramUtils.getVector(text));
				else if(type == 1)
					vectors.add(SkipUtils.getVector(text));	
			}
		}
		//System.out.println(vectors.size()+" "+type);
		double[] sum1 = ParagramUtils.add(vectors);

		vectors = new ArrayList<double[]>();
		for(int j=hi - offset; j <= hi + offset; j ++) {
			if( j >= 0 && j < hypCons.size()) {
				String text = hypCons.get(j).surfaceText;
				if(type == 0)
					vectors.add(ParagramUtils.getVector(text));
				else if(type == 1)
					vectors.add(SkipUtils.getVector(text));				
			}
		}
		double[] sum2 = ParagramUtils.add(vectors);

		return ParagramUtils.cosine(sum1, sum2);
	}

	public static double AverageScore(int type, PPDB ppdb, Example e, Constituent tc, Constituent hc, int offset, boolean excludestops) {

		ArrayList<Constituent> textCons = e.l_constituents;
		ArrayList<Constituent> hypCons = e.s_constituents;

		int ti = tc.start;
		int hi = hc.start;

		if(ti==textCons.size() || hi == hypCons.size() || ti < 0 || hi < 0)
			return 0.;

		String word_1 = textCons.get(ti).surfaceText;
		String word_2 = hypCons.get(hi).surfaceText;

		double total = 0;
		double ct = 0;
		for(int i=ti-offset; i <= ti + offset; i++) {
			for(int j=hi - offset; j <= hi + offset; j ++) {
				if( i >= 0 && j >= 0 && i < textCons.size() && j < hypCons.size()) {
					if(i==ti && j==hi)
						continue;
					double temp = NeighborhoodSim(type, ppdb, textCons.get(i), hypCons.get(j), excludestops);
					if(temp < 0)
						continue;
					total += temp;
					ct += 1;
				}
			}
		}

		if(ct < 1)
			return 0;

		return total/ct;
	}

	//helper for Neighborhood score
	public static double NeighborhoodSim(int type, PPDB ppdb, Constituent t, Constituent h, boolean excludestops) {
		String word_1 = t.surfaceText;
		String word_2 = h.surfaceText;

		if(excludestops && (Utils.isStop(word_1) || Utils.isStop(word_2)))
			return -1.;

		double score = 0;
		if(type == 0)
			score = FeatureFunctions.paragonSim(t, h);
		else if(type == 1)
			score = FeatureFunctions.skipSim(t, h);

		return score;
	}

	public static double paragonSim(Constituent tc, Constituent hc) {
		return ParagramUtils.paragramScore(tc.surfaceText, hc.surfaceText);
	}
}
