package edu.illinois.cs.features;

import java.io.Serializable;

public class Constituent implements Serializable {

	public int start;
	public int end;
	public String surfaceText;
	public String lemma;
	public String pos;
	public String ner;
	
	public String id;
}
