package edu.illinois.cs.features;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.readers.Example;

public class FeatureGenerator1 extends FeatureGenerator {

	public HashMap<String,Double> phrmap = new HashMap<String,Double>();
	public HashMap<String,Double> wdmap = new HashMap<String,Double>();

	public FeatureGenerator1(ResourceManager rm, String words, String phrases) throws FileNotFoundException, IllegalArgumentException {
		super(rm);
		
		if(!words.equals("null")) {
			// we are using word similarity features
			ArrayList<String> keys = LineIO.read(rm.getString("word")); //list of word pairs
			ArrayList<String> values = LineIO.read(words);
			for(int i=0; i < keys.size(); i++) {
				wdmap.put(keys.get(i).trim(),Double.parseDouble(values.get(i).trim()));
			}
		}
		
		if(!phrases.equals("null")) {
			// we are using phrase similarity features
			ArrayList<String> keys = LineIO.read(rm.getString("phrase")); //list of phrase pairs
			ArrayList<String> values = LineIO.read(phrases);
			for(int i=0; i < keys.size(); i++) {
				phrmap.put(keys.get(i).trim(),Double.parseDouble(values.get(i).trim()));
			}
		}
	}

	public Map<String, Double> getNodeAlignmentFeatures(Example e,
			Constituent textConstituent, Constituent hypothesisConstituent) {
		// TODO Auto-generated method stub

		return getFeatures(e,textConstituent, hypothesisConstituent);
	}

	public HashMap<String, Double> getFeatures(Example e, Constituent textConstituent,
			Constituent hypothesisConstituent) {

		HashMap<String, Double> map = new HashMap<String, Double>();

		String s = FeatureFunctions.wordWord(textConstituent,hypothesisConstituent);
		if(!rm.getBoolean("noword")) //word-word feature
			map.put(s, 1.0);
		else {
			//map.put("oneIsNull", checkIfNull(e,textConstituent,hypothesisConstituent));
		}

		map.put("word", getWordScore(e,textConstituent,hypothesisConstituent));
		map.put("phrase", getPhraseScore(e,textConstituent,hypothesisConstituent));

		return map;
	}
	
	public double checkIfNull(Example e, Constituent tc, Constituent hc) {
		
		int ti = tc.start;
		int hi = hc.start;
		
		ArrayList<Constituent> textCons = e.l_constituents;
		ArrayList<Constituent> hypCons = e.s_constituents;
		
		if(ti==textCons.size() || hi == hypCons.size() || ti < 0 || hi < 0)
			return 1.;
		
		return 0.;
	}
	
	/*
	 * This function just looks up the word pair in the cache and returns the
	 * score.
	 */
	public double getWordScore(Example e, Constituent tc, Constituent hc) {
		
		if(wdmap.size()==0)
			return 0.;
		
		ArrayList<Constituent> textCons = e.l_constituents;
		ArrayList<Constituent> hypCons = e.s_constituents;

		int ti = tc.start;
		int hi = hc.start;
		
		if(ti==textCons.size() || hi == hypCons.size() || ti < 0 || hi < 0)
			return 0;
		
		String s1 = tc.surfaceText;
		String s2 = hc.surfaceText;
		String key = "";
		
		if(s1.compareTo(s2) < 0) {
			key = s1+" "+s2;
			if(wdmap!=null && !wdmap.containsKey(key))
				key = s2+" "+s1;
		}
		else {
			key = s2+" "+s1;
			if(wdmap!=null && !wdmap.containsKey(key))
				key = s1+" "+s2;
		}
		
		assert wdmap.containsKey(key);
		if(!wdmap.containsKey(key))
			System.out.println("ERROR in WORDMAP");
		
		return wdmap.get(key);
	}
	
	/*
	 * This function just looks up the word pair in the cache and returns the
	 * score.
	 */
	public double getPhraseScore(Example e, Constituent tc, Constituent hc) {

		int offset = 2;
		
		if(phrmap.size()==0)
			return 0.;
		
		ArrayList<Constituent> textCons = e.l_constituents;
		ArrayList<Constituent> hypCons = e.s_constituents;

		int ti = tc.start;
		int hi = hc.start;

		if(ti==textCons.size() || hi == hypCons.size() || ti < 0 || hi < 0)
			return 0;

		String text_w = "";
		for(int i=ti-offset; i <= ti + offset; i++) {
			if( i >= 0 && i < textCons.size()) {
				String text = textCons.get(i).surfaceText;
				text_w += text+" ";
			}
		}
		text_w = text_w.trim();

		String hyp_w = "";
		for(int j=hi - offset; j <= hi + offset; j ++) {
			if( j >= 0 && j < hypCons.size()) {
				String text = hypCons.get(j).surfaceText;
				hyp_w += text+" ";
			}
		}
		hyp_w = hyp_w.trim();

		String key = "";
		String s1 = text_w.toLowerCase();
		String s2 = hyp_w.toLowerCase();
		if(s1.compareTo(s2) < 0) {
			key = s1+"|||"+s2+"|||1.0";
			if(phrmap!=null && !phrmap.containsKey(key))
				key = s2+"|||"+s1+"|||1.0";
		}
		else {
			key = s2+"|||"+s1+"|||1.0";
			if(phrmap!=null && !phrmap.containsKey(key))
				key = s1+"|||"+s2+"|||1.0";
		}
		
		assert phrmap.containsKey(key);
		
		return phrmap.get(key);
	}
}
