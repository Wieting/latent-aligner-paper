package edu.illinois.cs.features;

import java.util.ArrayList;

import edu.illinois.cs.readers.Example;
import edu.illinois.cs.utils.Constants;
import edu.illinois.cs.utils.ParagramUtils;
import edu.illinois.cs.utils.SkipUtils;
import edu.illinois.cs.utils.Utils;
import edu.illinois.cs.utils.WNUtils;

//one is a stop (done) Feature
//sim score (ppdb, paragram)
//neighborhood score (ppdb, paragram)
//WN siblings
//WN synonym
//WN hypernym
//WN antonym
//WN derivational form (not yet)
//both # and equal (done)
//both # and not equal (done)
//one is a punct (done)
//Null and POS ?
//POS-POS x
//word-word x
//idf ?

public class FeatureFunctions {

	public static boolean isSynonym(Constituent tc, Constituent hc) {
		return WNUtils.isSynonym(tc.surfaceText, tc.pos, hc.surfaceText, hc.pos);
	}

	public static boolean isAntonym(Constituent tc, Constituent hc) {
		return WNUtils.isAntonym(tc.surfaceText, tc.pos, hc.surfaceText, hc.pos);
	}
	
	public static boolean isHypernym(Constituent tc, Constituent hc) {
		boolean b1 = WNUtils.isHypernym(tc.surfaceText, tc.pos, hc.surfaceText, hc.pos);
		boolean b2 = WNUtils.isHypernym(tc.surfaceText, tc.pos, hc.surfaceText, hc.pos);
		
		return b1 || b2;
	}
	
	public static boolean isSiblingNotHypernym(Constituent tc, Constituent hc) {
		boolean b1 = WNUtils.isHypernym(tc.surfaceText, tc.pos, hc.surfaceText, hc.pos);
		boolean b2 = WNUtils.isHypernym(tc.surfaceText, tc.pos, hc.surfaceText, hc.pos);
		boolean b3 = WNUtils.isSibling(tc.surfaceText, tc.pos, hc.surfaceText, hc.pos, 2);
		
		return b3 && !b1 && !b2;
	}
	
	public static String universalPOS(Constituent tc, Constituent hc) {
		String pos1 = Utils.getUnivPOS(tc.pos);
		String pos2 = Utils.getUnivPOS(hc.pos);
		
		if(pos1.compareTo(pos2) < 0)
			return "POS-"+pos1+"-"+pos2;
		
		return "POS-"+pos2+"-"+pos1;
	}
	
	public static String wordWord(Constituent tc, Constituent hc) {

		String lemma1 = tc.lemma;
		String lemma2 = hc.lemma;
		
		if(lemma1.compareTo(lemma2) < 0)
			return "LEMMA-"+lemma1.toLowerCase()+"-"+lemma2.toLowerCase();
		
		return "LEMMA-"+lemma2.toLowerCase()+"-"+lemma1.toLowerCase();
	}
	
	public static double paragonSim(Constituent tc, Constituent hc) {
		String word_1 = tc.surfaceText;
		String word_2 = hc.surfaceText;
		if(word_1.equals(Constants.null_text) || word_2.equals(Constants.null_text))
			return 0.;
		return ParagramUtils.paragramScore(tc.surfaceText, hc.surfaceText);
	}

	public static double skipSim(Constituent tc, Constituent hc) {
		String word_1 = tc.surfaceText;
		String word_2 = hc.surfaceText;
		if(word_1.equals(Constants.null_text) || word_2.equals(Constants.null_text))
			return 0.;
		return SkipUtils.paragramScore(tc.surfaceText, hc.surfaceText);
	}
	
	public static double ParagramNeighborhoodScore(PPDB ppdb, Example e, Constituent tc, Constituent hc, int offset, boolean excludestops) {

		ArrayList<Constituent> textCons = e.l_constituents;
		ArrayList<Constituent> hypCons = e.s_constituents;

		int ti = tc.start;
		int hi = hc.start;

		if(ti==textCons.size() || hi == hypCons.size() || ti < 0 || hi < 0)
			return 0.;

		String word_1 = textCons.get(ti).surfaceText;
		String word_2 = hypCons.get(hi).surfaceText;

		double total = 0;
		double ct = 0;
		for(int i=ti-offset; i <= ti + offset; i++) {
			for(int j=hi - offset; j <= hi + offset; j ++) {
				if( i >= 0 && j >= 0 && i < textCons.size() && j < hypCons.size()) {
					if(i==ti && j==hi)
						continue;
					double temp = ParagramNeighborhoodSim(ppdb, textCons.get(i), hypCons.get(j), excludestops);
					if(temp < 0)
						continue;
					total += temp;
					ct += 1;
				}
			}
		}

		if(ct < 1)
			return 0;

		return total/ct;
	}

	//helper for Neighborhood score
	public static double ParagramNeighborhoodSim(PPDB ppdb, Constituent t, Constituent h, boolean excludestops) {
		String word_1 = t.surfaceText;
		String word_2 = h.surfaceText;
		
		if(excludestops && (Utils.isStop(word_1) || Utils.isStop(word_2)))
			return -1.;
		
		double score = FeatureFunctions.paragonSim(t, h);
		
		return score;
	}
}
