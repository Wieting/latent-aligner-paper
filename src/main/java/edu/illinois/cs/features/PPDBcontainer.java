package edu.illinois.cs.features;

import java.util.ArrayList;
import java.util.HashMap;

import edu.stanford.nlp.util.Pair;

public class PPDBcontainer {

	public ArrayList<Pair<Constituent,Constituent>> lexmap = new ArrayList<Pair<Constituent,Constituent>>();
	public ArrayList<Pair<Constituent,Constituent>> phrmap = new ArrayList<Pair<Constituent,Constituent>>();
	public ArrayList<Pair<Constituent,Constituent>> o2mmap = new ArrayList<Pair<Constituent,Constituent>>();
	
}
