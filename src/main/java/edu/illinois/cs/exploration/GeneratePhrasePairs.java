package edu.illinois.cs.exploration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.features.Constituent;
import edu.illinois.cs.features.PPDB;
import edu.illinois.cs.readers.Example;
import edu.illinois.cs.readers.ExampleGetter;
import edu.illinois.cs.utils.ConfigParams;
import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class GeneratePhrasePairs {
	public static void main(String[] args) throws Exception {

		String configfile = "config/learn.ent.txt";
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		props.put("tokenize.options", "americanize=true");
		props.put("ssplit.isOneSentence", "true");

		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		
		ResourceManager rm = new ResourceManager(configfile);
		PPDB ppdb = new PPDB(rm,pipeline);
		
		ExampleGetter eg = new ExampleGetter(rm,ppdb,pipeline);
    	ArrayList<Example> examples = eg.test;
    	examples.addAll(eg.dev);
    	examples.addAll(eg.train);

		printWordPairs(examples);
	}

	public static void printWordPairs(ArrayList<Example> examples) throws IOException {

		HashSet<String> wordpairs = new HashSet<String>();

		for(Example e : examples) {
			ArrayList<Constituent> cons1 = e.l_constituents;
			ArrayList<Constituent> cons2 = e.s_constituents;

			for(int i=0; i < cons1.size(); i++) {
				Constituent c1 = cons1.get(i);
				for(int j=0; j < cons2.size(); j++) {
					Constituent c2 = cons2.get(j);
					String s1 = "";
					String s3 = "";
					String w1 = c1.surfaceText;
					String w2 = c2.surfaceText;
					String con_w1_1 = getContext(i,cons1,2);
					String con_w2_1 = getContext(j,cons2,2);
					//String con_w1_3 = getContext(i,cons1,3);
					//String con_w2_3 = getContext(j,cons2,3);
					if(w1.compareTo(w2) < 0) {
						s1 = con_w1_1+"|||"+con_w2_1+"|||1.0";
						//s3 = con_w1_3+"|||"+con_w2_3+"|||1.0";
					}
					else {
						s1 = con_w2_1+"|||"+con_w1_1+"|||1.0";
						//s3 = con_w2_3+"|||"+con_w1_3+"|||1.0";
					}
					wordpairs.add(s1);
					wordpairs.add(s3);
				}
			}
		}

		//System.out.println(wordpairs.size());
		LineIO.write("outputs/phrase_pairs", new ArrayList<String>(wordpairs));
	}

	private static String getContext(int i, ArrayList<Constituent> cons, int n) {
		String context = "";
		for(int ii = i-n; ii <= i+n; ii++) {
			if(ii < 0 || ii >= cons.size()) {
				continue;
			}
			context += cons.get(ii).surfaceText.toLowerCase()+" ";
		}
		return context.trim();
	}
}
