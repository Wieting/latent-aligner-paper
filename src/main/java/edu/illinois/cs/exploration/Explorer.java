package edu.illinois.cs.exploration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import edu.illinois.cs.features.PPDB;
import edu.illinois.cs.readers.Example;
import edu.illinois.cs.utils.Utils;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.util.CoreMap;

public class Explorer {

	public ArrayList<Example> examples;
	public HashSet<String> stopwords;
	public String corpus;

	public Explorer(ArrayList<Example> examples) {
		this.examples = examples;
		stopwords = Utils.loadStopWords();
		corpus = examples.get(0).corpus;
	}

	public void exploreNEs() {
		for(Example e: examples) {
			for (CoreLabel token: e.l_map.get(TokensAnnotation.class)) {
				String word = token.get(TextAnnotation.class);
				String ne = token.get(NamedEntityTagAnnotation.class);
				System.out.println(word+" "+ne);
			}
			
			System.out.println();
			
			for (CoreLabel token: e.s_map.get(TokensAnnotation.class)) {
				String word = token.get(TextAnnotation.class);
				String ne = token.get(NamedEntityTagAnnotation.class);
				System.out.println(word+" "+ne);
			}
			
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
		}
	}
	
	public void evaluatePPDB() {
    	//PPDB ppdb = new PPDB();
	}
	
	public void evaluateNGrams(boolean usestops, boolean excludepuncts) {

		int[] n2grams = new int[3];
		int[] n3grams = new int[3];
		int[] n4grams = new int[3];
		int[] n5grams = new int[3];

		if(corpus.equals("SemEval")) {
			n2grams = new int[4];
			n3grams = new int[4];
			n4grams = new int[4];
			n5grams = new int[4];
		}

		for(int i=0; i<examples.size(); i++) {
			//System.out.println(i);
			List<CoreLabel> s_toks = examples.get(i).s_map.get(TokensAnnotation.class);
			for (int j=0; j < s_toks.size(); j++) {
				CoreLabel st = s_toks.get(j);
				List<CoreLabel> l_toks = examples.get(i).l_map.get(TokensAnnotation.class);
				for(int k=0; k < l_toks.size(); k++) {
					CoreLabel lt = l_toks.get(k);
					if(st.originalText().equals(lt.originalText())) {
						//System.out.println(examples.get(i).s_text);
						//System.out.println(examples.get(i).l_text);
						int n = getNGram(s_toks,j,l_toks,k,usestops, excludepuncts);
						if(n==2)
							addToArr(n2grams, examples.get(i).label, examples.get(i).s_label);
						if(n==3)
							addToArr(n3grams, examples.get(i).label, examples.get(i).s_label);
						if(n==4)
							addToArr(n4grams, examples.get(i).label, examples.get(i).s_label);
						if(n>=5)
							addToArr(n5grams, examples.get(i).label, examples.get(i).s_label);

					}
				}
			}
		}

		printCounts(n2grams);
		printCounts(n3grams);
		printCounts(n4grams);
		printCounts(n5grams);
	}

	private void printCounts(int[] arr) {
		if(!corpus.equals("SemEval")) {
		double total = arr[1]+arr[2];
		System.out.println(arr[0]+" "+arr[1]+" "+arr[2]+" "+arr[1]/total+" "+arr[2]/total);
		}
		else {
			double total = arr[1]+arr[2]+arr[3];
			System.out.println(arr[0]+" "+arr[1]+" "+arr[2]+" "+arr[3]+" "+arr[1]/total+" "+arr[2]/total+" "+arr[3]/total);
		}
	}

	private void addToArr(int[] n2grams, int label, String s_label) {
		if(!corpus.equals("SemEval")) {
			n2grams[0] += 1;
			if(label > 0) {
				n2grams[1] += 1;
			}
			else {
				n2grams[2] += 1;
			}
		}
		else {
			n2grams[0] += 1;
			if(s_label.equals("ENTAILMENT")) {
				n2grams[1] += 1;
			}
			else if(s_label.equals("CONTRADICTION")){
				n2grams[2] += 1;
			}
			else {
				n2grams[3] += 1;
			}
		}
	}

	private int getNGram(List<CoreLabel> s_toks, int si,
			List<CoreLabel> l_toks, int li, boolean usestops, boolean excludepuncts) {

		int n = 0;
		int j = li;
		boolean nonstop = false;
		for(int i=si; i < s_toks.size(); i++) {
			if(j >= l_toks.size())
				break;

			CoreLabel s_tok = s_toks.get(i);
			CoreLabel l_tok = l_toks.get(j);
			String s_st = s_tok.originalText();
			String l_st = l_tok.originalText();

			if(excludepuncts && Utils.isPunct(s_st) && Utils.isPunct(l_st)) {
				j++;
				continue;
			}

			if(excludepuncts && Utils.isPunct(s_st)) {
				continue;
			}

			if(excludepuncts && Utils.isPunct(l_st)) {
				j++;
				continue;
			}


			if(s_st.equals(l_st)) {
				n++;
				if(!stopwords.contains(s_st)) {
					nonstop = true;
				}
			}
			else {
				break;
			}

			j++;
		}

		if(usestops && !nonstop)
			return 0;

		return n;
	}
}
