package edu.illinois.cs.exploration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Properties;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.features.Constituent;
import edu.illinois.cs.features.PPDB;
import edu.illinois.cs.readers.Example;
import edu.illinois.cs.readers.ExampleGetter;
import edu.illinois.cs.readers.MSRParaReader;
import edu.illinois.cs.readers.RTE5Reader;
import edu.illinois.cs.readers.SemEvalReader14;
import edu.illinois.cs.utils.Constants;
import edu.stanford.nlp.io.IOUtils;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class GenerateWordPairs {

	public static void main(String[] args) throws Exception {
    	
		String configfile = "config/learn.ent.txt";
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		props.put("tokenize.options", "americanize=true");
		props.put("ssplit.isOneSentence", "true");

		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		
		ResourceManager rm = new ResourceManager(configfile);
		PPDB ppdb = new PPDB(rm,pipeline);
		
		ExampleGetter eg = new ExampleGetter(rm,ppdb,pipeline);
    	ArrayList<Example> examples = eg.test;
    	examples.addAll(eg.dev);
    	examples.addAll(eg.train);
		
    	printWordPairs(examples);
	}
	
	public static void printWordPairs(ArrayList<Example> examples) throws IOException {
		
		HashSet<String> wordpairs = new HashSet<String>();
		
		for(Example e : examples) {
			ArrayList<Constituent> cons1 = e.l_constituents;
			ArrayList<Constituent> cons2 = e.s_constituents;
			
			for(Constituent c1: cons1) {
				for(Constituent c2 : cons2) {
					String s = "";
					String w1 = c1.surfaceText;
					String w2 = c2.surfaceText;
					String pos1 = c1.pos;
					String pos2 = c2.pos;
					if(w1.compareTo(w2) < 0) {
						//s = w1+"\t"+pos1+"\t"+w2+"\t"+pos2;
						s = w1+" "+w2;
					}
					else {
						//s = w2+"\t"+pos2+"\t"+w1+"\t"+pos1;
						s = w2+" "+w1;
					}
					wordpairs.add(s);
				}
			}
		}
		
		//System.out.println(wordpairs.size());
		LineIO.write("outputs/word_pairs", new ArrayList<String>(wordpairs));
	}
	
}
