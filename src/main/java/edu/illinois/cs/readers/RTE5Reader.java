package edu.illinois.cs.readers;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;
import edu.illinois.cs.utils.Utils;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RTE5Reader {

	public ArrayList<Example> examples;
	
    public RTE5Reader(String corpus, String file, boolean doAnnotation) throws Exception {
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma, ner");
		props.put("tokenize.options", "americanize=true");
		props.put("ssplit.isOneSentence", "true");

		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
    	
    	ArrayList<String> textStringsList = new ArrayList<String>();
        ArrayList<String> hypothesisStringsList = new ArrayList<String>();
        ArrayList<String> labels = new ArrayList<String>();
        try {
            for (String line : LineIO.read(file)) {
                line = line.trim();
                if (line.startsWith("<t>"))
                    textStringsList.add(Utils.cleanup(line.substring(3, line.indexOf("</t>"))));
                else if (line.startsWith("<h>"))
                    hypothesisStringsList.add(Utils.cleanup(line.substring(3, line.indexOf("</h>"))));
                else if (line.startsWith("<pair")) {
                    Matcher m = Pattern.compile(".*entailment=\"([A-Z]+)\".*").matcher(line);
                    if (m.matches()) {
                        labels.add(m.group(1));
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // Make sure we got the same number of T/H strings and pairs
        if (textStringsList.size() != hypothesisStringsList.size() || textStringsList.size() != labels.size()) {
            System.err.println("Error reading the corpus; not the same number of texts/hypotheses");
            return;
        }
        
        examples = new ArrayList<Example>();
        
        for(int i=0; i < textStringsList.size(); i++) {
        	int il = labels.get(i).equals("ENTAILMENT") ? 1 : -1;
        	Example e = new Example("RTE5", i, il, labels.get(i), textStringsList.get(i), hypothesisStringsList.get(i), pipeline, doAnnotation);
        	examples.add(e);
        }
    }
}

