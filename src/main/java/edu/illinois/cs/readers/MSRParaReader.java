package edu.illinois.cs.readers;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.features.PPDB;
import edu.illinois.cs.utils.Utils;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class MSRParaReader {

	public ArrayList<Example> examples;

	public MSRParaReader(String corpus, String file, boolean doAnnotation) throws Exception {
		Properties props = new Properties();
		//props.put("annotators", "tokenize, ssplit, pos, lemma, ner");
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		props.put("tokenize.options", "americanize=true");
		props.put("ssplit.isOneSentence", "true");

		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		examples = new ArrayList<Example>();

		try {
			int ct =0;
			for (String line : LineIO.read(file)) {
				if(ct==0) {
					ct += 1;
					continue;
				}
				
				line = line.trim();
				String[] arr = line.split("\t");
				String label = arr[0];
				String t1 = arr[3];
				String t2 = arr[4];
				int l = label.equals("1") ? 1 : -1;
				Example ee = new Example(corpus, ct, l, label, t1, t2, pipeline, doAnnotation);
				//ee.addNgrams(true,true);
				examples.add(ee);
				ct += 1;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public MSRParaReader(String corpus, String file, boolean doAnnotation, PPDB ppdb) throws Exception {
		Properties props = new Properties();
		//props.put("annotators", "tokenize, ssplit, pos, lemma, ner");
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		props.put("tokenize.options", "americanize=true");
		props.put("ssplit.isOneSentence", "true");

		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		examples = new ArrayList<Example>();

		try {
			int ct =0;
			for (String line : LineIO.read(file)) {
				if(ct==0) {
					ct += 1;
					continue;
				}
				
				line = line.trim();
				String[] arr = line.split("\t");
				String label = arr[0];
				String t1 = arr[3];
				String t2 = arr[4];
				int l = label.equals("1") ? 1 : -1;
				Example ee = new Example(corpus, ct, l, label, t1, t2, pipeline, doAnnotation);
				//ee.addNgrams(true,true);
				ee.addPPDB(ppdb);
				examples.add(ee);
				ct += 1;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
