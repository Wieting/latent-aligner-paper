package edu.illinois.cs.readers;

import java.io.Serializable;
import java.util.ArrayList;

import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.features.PPDB;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class ExampleGetter implements Serializable {

	public ArrayList<Example> train;
	public ArrayList<Example> test;
	public ArrayList<Example> dev;
	
	public ExampleGetter(ResourceManager rm, PPDB ppdb, StanfordCoreNLP pipeline) {
		
		String reader = rm.getString("reader");
		loadExamples(reader, rm, ppdb, pipeline);
	}

	private void loadExamples(String reader, ResourceManager rm, PPDB ppdb, StanfordCoreNLP pipeline) {
		String trainf = rm.getString("train").split("\\\\")[rm.getString("train").split("\\\\").length - 1];
		String testf = rm.getString("test").split("\\\\")[rm.getString("test").split("\\\\").length - 1];

		Reader r;
		
		if(reader.equals("semeval14entailment")) {
			String devf = rm.getString("dev").split("\\\\")[rm.getString("dev").split("\\\\").length - 1];
			try {
				r = new SemEvalReader14(trainf,testf,devf,ppdb,pipeline);
				train = r.train;
				test = r.test;
				dev = r.dev;
				setRegression(train);
				setRegression(test);
				setRegression(dev);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(reader.equals("semeval14similarity")) {
			String devf = rm.getString("dev").split("\\\\")[rm.getString("dev").split("\\\\").length - 1];
			try {
				r = new SemEvalReader14(trainf,testf,devf,ppdb,pipeline);
				train = r.train;
				test = r.test;
				dev = r.dev;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public static void setRegression(ArrayList<Example> data) {
		for(Example e: data) {
			if(e.s_label.equals("ENTAILMENT")) {
				e.simscore=2;
			}
			else if(e.s_label.equals("NEUTRAL")) {
				e.simscore=1;
			}
			else if(e.s_label.equals("CONTRADICTION")) {
				e.simscore=0;
			}
			else {
				System.out.println("ERROR..in ExampleGetter. Exiting.");
				System.exit(1);
			}
		}
	}
}
