package edu.illinois.cs.readers;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import edu.illinois.cs.features.Constituent;
import edu.illinois.cs.features.Ngrams;
import edu.illinois.cs.features.PPDB;
import edu.illinois.cs.features.PPDBcontainer;
import edu.illinois.cs.utils.Utils;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.Pair;

public class Example implements Serializable {

	public String corpus; //either MSR, SE, or RTE
	public int label; //1 or -1
	public String s_label;
	public String l_text; //longer sentence
	public String s_text; //shorter sentence
	public CoreMap l_map;
	public CoreMap s_map;
	public ArrayList<Constituent> l_constituents;
	public ArrayList<Constituent> s_constituents;
	public double simscore;
	public HashMap<String,ArrayList<Integer>> ngramMap = new HashMap<String,ArrayList<Integer>>();
	public HashMap<String,ArrayList<Integer>> ppdbMap = new HashMap<String,ArrayList<Integer>>();
	
	public int example_id;
	
	public Example(String corpus, int example_id, int l, String s_l, String l_t, String s_t, StanfordCoreNLP pipeline, boolean doAnnotation) {
		this.corpus = corpus;
		this.label = l;
		this.s_label = s_l;
		this.l_text = Utils.removePunct(pipeline, l_t);
		this.s_text = Utils.removePunct(pipeline, s_t);
		this.example_id = example_id;
		if(doAnnotation)
			annotate(pipeline);
		makeConstituents(doAnnotation);
	}
	
	private void makeConstituents(boolean doAnnotation) {
		if(!doAnnotation)
			return;
		
		List<CoreLabel> l_toks = l_map.get(TokensAnnotation.class);
		List<CoreLabel> s_toks = s_map.get(TokensAnnotation.class);
		
		l_constituents = new ArrayList<Constituent>();
		s_constituents = new ArrayList<Constituent>();
		
		if(l_toks.size() < s_toks.size())
			switchpairs();
		
		int idx = 0;
		for(CoreLabel c : l_toks) {
			Constituent con = new Constituent();
			con.start= idx;
			con.end = idx;
			con.surfaceText = c.originalText();
			con.pos = c.tag();
			con.ner = c.ner();
			con.lemma = c.lemma();
			con.id = corpus+":long:"+ example_id+":"+idx;
			l_constituents.add(con);
			idx++;
		}
		
		idx = 0;
		for(CoreLabel c : s_toks) {
			Constituent con = new Constituent();
			con.start= idx;
			con.end = idx;
			con.surfaceText = c.originalText();
			con.pos = c.tag();
			con.ner = c.ner();
			con.lemma = c.lemma();
			con.id = corpus+":short:"+ example_id+":"+idx;
			//System.out.println(con.pos+" "+con.ner);
			s_constituents.add(con);
			idx++;
		}
	}

	public void switchpairs() {
		String temp = s_text;
		s_text = l_text;
		l_text = temp;
		
		CoreMap m = s_map;
		s_map = l_map;
		l_map = m;
		
		ArrayList<Constituent> t_lis = s_constituents;
		s_constituents = l_constituents;
		l_constituents = t_lis;
	}
	
	public void annotate(StanfordCoreNLP pipeline) {
		Annotation document = new Annotation(l_text);
		pipeline.annotate(document);
		List<CoreMap> sentences = document.get(SentencesAnnotation.class);
		l_map = sentences.get(0);
		
		document = new Annotation(s_text);
		pipeline.annotate(document);
		sentences = document.get(SentencesAnnotation.class);
		s_map = sentences.get(0);
	}
	
	public void addPPDB(PPDB ppdb) {
		PPDBcontainer c = ppdb.analyzeSentencePair(this);
		ArrayList<Pair<Constituent,Constituent>> phrmap = c.phrmap;
		
		for(int i=0; i < phrmap.size(); i++) {
			Constituent f = phrmap.get(i).first;
			Constituent s = phrmap.get(i).second;
			
			int s1 = f.surfaceText.split("\\s+").length;
			int s2 = s.surfaceText.split("\\s+").length;
			int ss = Math.max(s1, s2);
			
			String key = s.start+":"+s.end+" "+f.start+":"+f.end; //short long
			
			for(int j=s.start; j<=s.end; j++) {
				for(int k=f.start; k<=f.end; k++) {
					key = j+":"+k;
					addToMap(ppdbMap,key,ss);
				}
			}
		}
	}
	
	public static void addToMap(HashMap<String,ArrayList<Integer>> map, String k, int v) {
		ArrayList<Integer> temp = new ArrayList<Integer>();
		if(map.containsKey(k))
			temp = map.get(k);
		temp.add(v);
		map.put(k, temp);
	}
	
	public void addNgrams() {
		Ngrams ng = new Ngrams(Utils.loadStopWords());
		ng.analyzeSentencePair(this);
		
		//second is long
		for(Pair<Constituent,Constituent> p : ng.twogram_map) {
			Constituent s = p.first();
			Constituent f = p.second();
			String key = s.start+":"+s.end+" "+f.start+":"+f.end; //short long
			
			for(int j=s.start; j<=s.end; j++) {
				for(int k=f.start; k<=f.end; k++) {
					key = j+":"+k;
					addToMap(ngramMap,key,2);
				}
			}
		}
    	
    	for(Pair<Constituent,Constituent> p : ng.threegram_map) {
			Constituent s = p.first();
			Constituent f = p.second();
			String key = s.start+":"+s.end+" "+f.start+":"+f.end; //short long

			for(int j=s.start; j<=s.end; j++) {
				for(int k=f.start; k<=f.end; k++) {
					key = j+":"+k;
					addToMap(ngramMap,key,3);
				}
			}
    	}
    	
    	for(Pair<Constituent,Constituent> p : ng.fourgram_map) {
			Constituent s = p.first();
			Constituent f = p.second();
			String key = s.start+":"+s.end+" "+f.start+":"+f.end; //short long
			
			for(int j=s.start; j<=s.end; j++) {
				for(int k=f.start; k<=f.end; k++) {
					key = j+":"+k;
					addToMap(ngramMap,key,4);
				}
			}
    	}
    	
    	for(Pair<Constituent,Constituent> p : ng.fivegram_map) {
			Constituent s = p.first();
			Constituent f = p.second();
			String key = s.start+":"+s.end+" "+f.start+":"+f.end; //short long

			for(int j=s.start; j<=s.end; j++) {
				for(int k=f.start; k<=f.end; k++) {
					key = j+":"+k;
					addToMap(ngramMap,key,5);
				}
			}
    	}
	}
	
}
