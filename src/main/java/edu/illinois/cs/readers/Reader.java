package edu.illinois.cs.readers;

import java.util.ArrayList;
import java.util.Properties;

import edu.illinois.cs.features.PPDB;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public abstract class Reader {
	
	public StanfordCoreNLP pipeline;
	public ArrayList<Example> train;
	public ArrayList<Example> test;
	public ArrayList<Example> dev;
	public PPDB ppdb;
	
	public Reader() {
	}
	
	abstract public void populateTrainTestDev(String trainf, String testf, String devf);
}
