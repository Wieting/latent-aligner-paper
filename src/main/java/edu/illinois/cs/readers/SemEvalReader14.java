package edu.illinois.cs.readers;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Properties;

import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.features.PPDB;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

public class SemEvalReader14 extends Reader {

	public SemEvalReader14(String trainf, String testf, String devf, PPDB ppdb, StanfordCoreNLP pipeline) throws Exception {
		this.ppdb = ppdb;
		this.pipeline = pipeline;
		populateTrainTestDev(trainf,testf,devf);
	}

	@Override
	public void populateTrainTestDev(String trainf, String testf, String devf) {
		train = getExamples(trainf,"SE14train");
		test = getExamples(testf,"SE14test");
		dev = getExamples(devf,"SE14dev");
	}

	public ArrayList<Example> getExamples(String file, String name) {
		
		ArrayList<Example> examples = new ArrayList<Example>();
		
		try {
			int ct =0;
			for (String line : LineIO.read(file)) {
				if(ct==0) {
					ct += 1;
					continue;
				}
				
				line = line.trim();
				String[] arr = line.split("\t");
				String label = arr[4];
				String t1 = arr[1];
				String t2 = arr[2];
				int l = label.equals("ENTAILMENT") ? 1 : -1;
				Example ee = new Example(name, ct, l, label, t1, t2, pipeline, true);
				ee.simscore = Double.parseDouble(arr[3]);
				//ee.addPPDB(ppdb);
				//ee.addNgrams();
				examples.add(ee);
				ct += 1;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return examples;
	}
}
