package edu.illinois.cs.main;

import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.features.PPDB;
import edu.illinois.cs.learning.Algorithms;
import edu.illinois.cs.readers.Example;
import edu.illinois.cs.readers.ExampleGetter;
import edu.illinois.cs.utils.ConfigParams;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;
import java.util.Random;

public class Main {

	/*
	 * Arguments:
	 * 		0 - config file
	 * 		1 = wordsim file (list of scores for word pairs or null if no phrase scores are used)
	 * 		2 - phrasesim file (list of scores for phrases or null if no phrase scores are used)
	 * 		3 - set to true if you want to train on 500 examples, otherwise set to something else
	 * 		4 - set to true if you want to print alignments
	 * 		5 - learning rate
	 */
	public static void main( String[] args ) throws Exception
	{	
		Algorithms.rand = new Random(1);
		
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		props.put("tokenize.options", "americanize=true");
		props.put("ssplit.isOneSentence", "true");

		StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
		
		ResourceManager rm = new ResourceManager(args[0]);
		PPDB ppdb = null;
		ExampleGetter eg = new ExampleGetter(rm,ppdb,pipeline);

		System.out.println("Training size "+eg.train.size());
		System.out.println("Developmenst size "+eg.dev.size());
		System.out.println("Test size "+eg.test.size());
		
		//train on 500 examples
		if(args.length >= 4 && args[3].equals("true")) {
			Collections.shuffle(eg.train,Algorithms.rand);
			ArrayList<Example> temp = new ArrayList<Example>();
			for(int i=0; i < 500; i++) {
				temp.add(eg.train.get(i));
			}
			eg.train = temp;
		}

		int iter = rm.getInt("iter");
		ConfigParams.R = Double.parseDouble(args[5]);
		
		if(rm.getString("learner").equals("lrp")) {
			//regularization parameters
			double[] paramsLA = {1E-5, 1E-6, 1E-4, 0.};
			if(args[0].contains("noword")) {
				paramsLA = new double[1];
				paramsLA[0]=0.;
				System.out.println("HERE");
			}
			//double[] paramsLA = {1E-6};
			for(double dd: paramsLA) {
				Algorithms.LatentRegressionPerceptron(dd,iter,eg.train,eg.test,eg.dev,rm,args[1],args[2]);
			}
		}
		if(rm.getString("learner").equals("lclr")) {
			//C parameters
			double[] paramsLCLR = {2E-2, 2E-1, 1, 2E1, 2E2};
			for(double dd: paramsLCLR) {
				Algorithms.LCLR(dd,iter,eg.train,eg.test,eg.dev,rm,args[1],args[2]);
			}
		}
	}
}
