package edu.illinois.cs.learning;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;
import edu.illinois.cs.readers.Example;
import edu.illinois.cs.utils.ConfigParams;
import edu.illinois.cs.utils.LearningUtils;
import edu.stanford.nlp.util.Pair;

public class RegularizedRegressionPerceptron extends WeightVector {

	public RegularizedRegressionPerceptron(ResourceManager rm) {		
		super(rm);
	}

	//max(0,1-w1.x + w2.x) + max(0,1-w1.x + w2.x) + \lambda/2*(w1+w2+w2)^2
	public void train(Aligner aligner, List<Integer> trainLabels,
			List<Example> trainingData, int outerIters, double lambda, ArrayList<Example> test_data,
			ArrayList<Example> dev_data) {

		double R = 0.05;
		R = ConfigParams.R;
		int batchsize = rm.getInt("batchsize","1");
		System.out.println("Using batchsize of: "+batchsize);
		
		for(int k=0; k < outerIters; k++) {

			Collections.shuffle(trainingData,Algorithms.rand);
			
			for (int i = 0; i < trainingData.size();) {
				
				double[] grad = new double[w.length];
				int count = 0;
				for(int j=i; j <i+batchsize && j < trainingData.size(); j++) {
					grad = VMUtils.add(grad,getGradient(aligner, trainingData.get(j)));
					count += 1;
				}

				grad = VMUtils.multiply(grad, 1./count);
				
				for(int j=0; j < w.length; j++) {
					grad[j] = grad[j] + lambda*w[j];
				}
				
				for(int j=0; j < w.length; j++) {
					w[j] = w[j] - R*grad[j];
				}
				
				i = i + batchsize;
				//System.out.println(i);
			}

			//evaluate
			if(k%1 == 0) {
				if(rm.getString("evaluation").equals("entailment")) {
					//LearningUtils.writeWV(w, aligner.lm);
					//System.exit(1);
					double[] testScore = predictScoreByFindingFeatures(aligner, test_data);
					Pair<Double,int[]> p = LearningUtils.evaluateEntailment(test_data, testScore);

					double[] devScore = predictScoreByFindingFeatures(aligner, dev_data);
					Pair<Double,int[]> p2 = LearningUtils.evaluateEntailment(dev_data, devScore);
					
					System.out.println("iter: "+k+" dev_accuracy\t"+p2.first+"\ttest_accuracy\t"+p.first);
					
					if(k%20==0) {
						//LearningUtils.writeWV(w, aligner.lm);
					}
				}
				else if(rm.getString("evaluation").equals("similarity")) {
					//LearningUtils.writeWV(w, aligner.lm);
					double[] testScore = predictScoreByFindingFeatures(aligner, test_data);
					Pair<double[],int[]> p = LearningUtils.evaluateSimilarity(test_data, testScore);

					double[] devScore = predictScoreByFindingFeatures(aligner, dev_data);
					Pair<double[],int[]> p2 = LearningUtils.evaluateSimilarity(dev_data, devScore);

					System.out.println("iter: "+k+" dev_sp_correlation\t"+p2.first[0]+"\ttest_sp_correlation\t"+p.first[0]+"\tdev_ps_correlation\t"+p2.first[1]+"\ttest_ps_correlation\t"+p.first[1]+"\tdev_mse\t"+p2.first[2]+"\ttest_mse\t"+p.first[2]);
				}
			}
		}
	}
	
	public double[] getGradient(Aligner aligner, Example e) {
		Feature[] x = aligner.getBestFeatures(e,this);

		double dp = dotProduct(x);

		double[] xx = new double[w.length];
		for (Feature fn : x) {
			xx[fn.getIndex()-1] = fn.getValue();
		}

		double y = e.simscore;

		double difference = (dp-y);
		//System.out.println(y+" "+dp+" "+difference);

		double[] grad = new double[w.length];
		
		for(int j=0; j < w.length; j++) {
			grad[j] = difference*xx[j];
			//System.out.println(difference*xx[j]+lambda*w[j]);
		}
		
		return grad;
	}

}
