package edu.illinois.cs.learning;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.cogcomp.core.utilities.StringUtils;
import edu.illinois.cs.features.FeatureGenerator;
import edu.illinois.cs.features.PPDB;
import edu.illinois.cs.readers.Example;
import edu.illinois.cs.utils.LearningUtils;

public class Algorithms {

	public static Random rand;
	
	public static void LatentRegressionPerceptron(double lambda, int iter, ArrayList<Example> train_data, ArrayList<Example> test_data, ArrayList<Example> dev_data, ResourceManager rm, String words, String phrases) {
		
        LexManager lm = new LexManager();
        RegularizedRegressionPerceptron mp = new RegularizedRegressionPerceptron(rm);
        
        FeatureGenerator fea = LearningUtils.getFG(rm, words, phrases);
        
        Aligner featureFinder = new Aligner(fea, lm, rm);

        lm.previewFeature("word");
		mp.setWElement(lm.getFeatureID("word"), 1.0);
        lm.previewFeature("phrase");
		mp.setWElement(lm.getFeatureID("phrase"), 1.0);
		
        List<Integer> trainLabels = new ArrayList<Integer>();
        for (Example tep : train_data) {
        	trainLabels.add(tep.label);
        }

        mp.train(featureFinder, trainLabels, train_data, iter, lambda, test_data, dev_data);

        fea.closeCache();
    }
	
	public static void LatentPerceptron(double lambda, int iter, ArrayList<Example> train_data, ArrayList<Example> test_data, ArrayList<Example> dev, ResourceManager rm, String words, String phrases) {

		LexManager lm = new LexManager();
		RegularizedPerceptron wv = new RegularizedPerceptron(rm);

        lm.previewFeature("word");
		wv.setWElement(lm.getFeatureID("word"), 1.0);
        lm.previewFeature("phrase");
		wv.setWElement(lm.getFeatureID("word"), 1.0);
		
        FeatureGenerator fea = LearningUtils.getFG(rm, words, phrases);
        
        if(rm.getBoolean("initweights","true"))
        	LearningUtils.initWeights(wv,lm, fea);

		Aligner featureFinder = new Aligner(fea, lm, rm);

		List<Integer> trainLabels = new ArrayList<Integer>();
		for (Example tep : train_data) {
			trainLabels.add(tep.label);
		}

		wv.train(featureFinder, trainLabels, train_data, iter, lambda, test_data, dev);

		fea.closeCache();
	}

	
	public static void LCLR(double C, int iter, ArrayList<Example> train_data, ArrayList<Example> test_data, ArrayList<Example> dev, ResourceManager rm, String words, String phrases) {

		LexManager lm = new LexManager();
		WeightVector wv = new WeightVector(rm);

        lm.previewFeature("word");
		wv.setWElement(lm.getFeatureID("word"), 1.0);
        lm.previewFeature("phrase");
		wv.setWElement(lm.getFeatureID("word"), 1.0);
        
		FeatureGenerator fea = LearningUtils.getFG(rm, words, phrases);
        
        //if(rm.getBoolean("initweights","true"))
        //	LearningUtils.initWeights(wv,lm, fea);
		
		Aligner featureFinder = new Aligner(fea, lm, rm);

		List<Integer> trainLabels = new ArrayList<Integer>();
		for (Example tep : train_data) {
			trainLabels.add(tep.label);
		}

		wv.updateWeightVectorWithFullCDLatentSVM(featureFinder, trainLabels, train_data, iter, C, test_data, dev);

		fea.closeCache();
	}
}
