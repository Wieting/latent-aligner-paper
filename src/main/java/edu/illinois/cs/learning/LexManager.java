package edu.illinois.cs.learning;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class LexManager {
    // be careful, start from zero
    public Map<String, Integer> feaStr2Id_map = new HashMap<String, Integer>();
    public Map<Integer, String> feaId2Str_map = new HashMap<Integer, String>();

    public LexManager() {
        // Used during ILPAligner.getBestFeatures()
        //previewFeature("*global-bias*");
    }

    public int getFeatureID(String s) {
        assert feaStr2Id_map.containsKey(s);
        return feaStr2Id_map.get(s);
    }

    public void previewFeature(String s) {
        if (!feaStr2Id_map.containsKey(s)) {
            int v = feaStr2Id_map.size();
            feaStr2Id_map.put(s, v);
            feaId2Str_map.put(v, s);
        }
    }

    public Feature[] convertRawFeaMap2LRFeatures(Map<String, Double> ex_fea_map) {
        Feature[] res = new Feature[ex_fea_map.size()];

        int idx = 0;
        for (String s : ex_fea_map.keySet()) {
            if (!feaStr2Id_map.containsKey(s)) {
                int v = feaStr2Id_map.size();
                feaStr2Id_map.put(s, v);
                feaId2Str_map.put(v, s);
            }

            int id = feaStr2Id_map.get(s);
            res[idx] = new Feature(id + 1, ex_fea_map.get(s), s);
            idx += 1;
        }

        Arrays.sort(res, new Comparator<Feature>() {
            public int compare(Feature o1, Feature o2) {
                if (o1.getIndex() < o2.getIndex())
                    return -1;
                else if (o1.getIndex() > o2.getIndex())
                    return 1;
                else
                    return 0;
            }
        });

        return res;
    }
}
