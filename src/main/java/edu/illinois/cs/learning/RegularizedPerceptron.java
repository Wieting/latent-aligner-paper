package edu.illinois.cs.learning;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.readers.Example;
import edu.illinois.cs.utils.LearningUtils;
import edu.stanford.nlp.util.Pair;

public class RegularizedPerceptron extends WeightVector {

	public RegularizedPerceptron(ResourceManager rm) {
		super(rm);
	}

	//max(0,1-y.x) + lambda/2 w^2
	public void train(Aligner aligner, List<Integer> trainLabels,
			List<Example> trainingData, int outerIters, double lambda, ArrayList<Example> test_data,
			ArrayList<Example> dev_data) {

		double R = 0.05;

		for(int k=0; k < outerIters; k++) {
			
			for (int i = 0; i < trainingData.size(); i++) {

				Feature[] x = aligner.getBestFeatures(trainingData.get(i), this);
				double dot_product = 0.0;

				for (Feature fn : x) {
					dot_product += w[fn.getIndex() - 1] * fn.getValue();
				}
				
				double[] xx = new double[w.length];
				for (Feature fn : x) {
					xx[fn.getIndex()-1] = fn.getValue();
				}

				double t = trainLabels.get(i)*dot_product;

				for(int j=0; j < w.length; j++) {
					if( 1-t <= 0 ) {
						w[j] -= R*lambda*w[j];
					}
					else {
						w[j] += R*(trainLabels.get(i)*xx[j] - lambda*w[j]);
					}
				}
				System.out.println(i);
			}
			
			//evaluate
			if(k%1 == 0) {
				double[] testScore = predictScoreByFindingFeatures(aligner, test_data);
				Pair<Double,int[]> p = LearningUtils.evaluate(test_data, testScore);

				double[] devScore = predictScoreByFindingFeatures(aligner, dev_data);
				Pair<Double,int[]> p2 = LearningUtils.evaluate(dev_data, devScore);
				
				System.out.println("iter: "+k+" dev_accuracy\t"+p2.first+"\ttest_accuracy\t"+p.first);
			}
		}
	}
}
