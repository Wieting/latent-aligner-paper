package edu.illinois.cs.learning;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import edu.illinois.cs.cogcomp.core.datastructures.Pair;
import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.cogcomp.lbjava.infer.GurobiHook;
import edu.illinois.cs.cogcomp.lbjava.infer.ILPSolver;
import edu.illinois.cs.features.Constituent;
import edu.illinois.cs.features.FeatureGenerator;
import edu.illinois.cs.readers.Example;
import edu.illinois.cs.utils.Constants;

public class Aligner {

	private FeatureGenerator fea;
	public LexManager lm;
	private ILPSolver xmp = new GurobiHook();
	
	public static Constituent nullWord;
	public ResourceManager rm = null;
	
	private Hashtable<Integer,Pair<Constituent,Constituent>> wordVariableMap = new Hashtable<Integer,Pair<Constituent,Constituent>>();
	private Hashtable<Pair<Constituent,Constituent>,Integer> variableWordMap = new Hashtable<Pair<Constituent,Constituent>,Integer>();
	
    public Aligner(FeatureGenerator fea, LexManager lm, ResourceManager rm) {
    	this.fea = fea;
    	this.lm = lm;
    	
    	nullWord = new Constituent();
    	
    	nullWord.start = Constants.null_start;
    	nullWord.end = Constants.null_end;
    	nullWord.surfaceText = Constants.null_text;
    	nullWord.pos = Constants.null_pos;
    	nullWord.ner = Constants.null_ner;
    	nullWord.lemma = Constants.null_text;
    	this.rm = rm;
    }

    public Alignment align(Example e, WeightVector wv) {
       
		wordVariableMap.clear();
		variableWordMap.clear();
        xmp.reset();

        List<Constituent> textNodes = e.l_constituents;
        List<Constituent> hypNodes = e.s_constituents;

		Hashtable<Integer, Vector<Integer>> cmap = new Hashtable<Integer, Vector<Integer>>();
        
        for (int i=0; i < hypNodes.size()+1; i++) {
			int[] ids = new int[textNodes.size() + 1];
			double[] coefs = new double[textNodes.size() + 1];
        	for (int j=0; j < textNodes.size()+1; j++) {
		
        		Constituent sw = null;
        		Constituent lw = null;
        		
				if (i < hypNodes.size())
					sw = hypNodes.get(i);
				else
					sw = nullWord;

				if (j < textNodes.size())
					lw = textNodes.get(j);
				else
					lw = nullWord;
				
				//do not allow null to null
				if (i < hypNodes.size() || j < textNodes.size()) {
					double weight = 0.;
					
					weight = wv.dotProduct(lm.convertRawFeaMap2LRFeatures(
							fea.getNodeAlignmentFeatures(e,lw,sw)));

					int varId = xmp.addBooleanVariable(weight);
					wordVariableMap.put(varId, new Pair<Constituent, Constituent>(sw, lw));
					variableWordMap.put(new Pair<Constituent, Constituent>(sw, lw), varId);

					ids[j] = varId;
					coefs[j] = 1;

					if (j < textNodes.size()) {
						if (cmap.get(j) == null)
							cmap.put(j, new Vector<Integer>());
						cmap.get(j).add(varId);
					}
				}
			}
			if (i < hypNodes.size())
				xmp.addEqualityConstraint(ids, coefs, 1);
		}

		for (Integer wid : cmap.keySet()) {
			Vector<Integer> idsVec = cmap.get(wid);
			int[] ids = new int[idsVec.size()];
			double[] coefs = new double[idsVec.size()];
			for (int i = 0; i < idsVec.size(); i++) {
				ids[i] = idsVec.get(i);
				coefs[i] = 1;
			}
			xmp.addEqualityConstraint(ids, coefs, 1);
		}
			
		xmp.setMaximize(true);
		boolean solved;
		try {
			solved = xmp.solve();
			if(!solved) {
				System.err.println("Unsat");
				return null;
			}
			
			Alignment alignment = new Alignment(e,fea);
			
			for(Integer varId:wordVariableMap.keySet()){
				boolean varIDres = xmp.getBooleanValue(varId);
				if(varIDres){
					Pair<Constituent, Constituent> pair = wordVariableMap.get(varId);
					alignment.addNodeAlignmentFromHypothesisToText(pair.getFirst(), pair.getSecond());
				}
			}
			
			return alignment;
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return null;
		}
    }
    
    private double[] getCoefficientArray(int size) {
        double[] result = new double[size];
        Arrays.fill(result, 1);
        return result;
    }

	/**
     * Performs alignment and returns the feature vector of the best aligned nodes/edges. Used during training/inference.
     * @param pair The Example to extract features from
     * @return The feature vector of the best alignment
     */
	public Feature[] getBestFeatures(Example example, WeightVector wv) {

        Alignment alignment;

        Map<String, Double> featureMap = new HashMap<String, Double>();

        int n_round = 1;
        if (example.label > 0) {
            for (int t = 0; t < n_round; t++) {
                alignment = align(example, wv);
                Map<String, Double> single_fea_map = alignment.getMapFeatures(fea);
                //single_fea_map.put("*global-bias*", 1.0);
                for (String s : single_fea_map.keySet()) {
                    if (!featureMap.containsKey(s))
                        featureMap.put(s, single_fea_map.get(s));
                    featureMap.put(s, featureMap.get(s) + single_fea_map.get(s));
                }
            }

            for (String s : featureMap.keySet()) {
                featureMap.put(s, featureMap.get(s) / n_round);
            }
        }
        else {
            alignment = align(example,wv);
            featureMap = alignment.getMapFeatures(fea);
            //featureMap.put("*global-bias*", 1.0);
        }
        
        featureMap.put("*global-bias*", 1.0);
        
        return lm.convertRawFeaMap2LRFeatures(featureMap);
	}
}
