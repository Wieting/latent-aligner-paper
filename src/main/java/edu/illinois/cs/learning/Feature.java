package edu.illinois.cs.learning;

/**
 * Created to replace the use of liblinear's FeatureNode
 */
public class Feature {
    private final int index;
    private final double value;
    public String name;
    
    public Feature(int index, double value, String name) {
        if(index < 1) {
            throw new IllegalArgumentException("index must be >= 1");
        } else {
            this.index = index;
            this.value = value;
            this.name = name;
        }
    }

    public int getIndex() {
        return index;
    }

    public double getValue() {
        return value;
    }
}
