package edu.illinois.cs.learning;

public class VMUtils {


	public static double[] matrixVector(double[][] matrix, double[] vector) {

		double[] output = new double[matrix.length];

		for(int i=0; i < matrix.length; i++) {
			for(int j=0; j < matrix[0].length; j++) {
				output[i] += matrix[i][j]*vector[j];
			}
		}

		return output;
	}

	public static double[] hadamond(double[] vector1, double[] vector2) {
		double[] output = new double[vector1.length];

		for(int i=0; i < vector1.length; i++) {
			output[i] = vector1[i]*vector2[i];
		}

		return output;
	}

	public static double[][] tanh(double[][] matrix) {
		double[][] output = new double[matrix.length][matrix[0].length];

		for(int i=0; i < matrix.length; i++) {
			for(int j=0; j < matrix[0].length; j++) {
				output[i][j] = Math.tanh(matrix[i][j]);
			}
		}
		return output;
	}

	public static double[] tanh(double[] vec) {
		double[] output = new double[vec.length];

		for(int i=0; i < vec.length; i++) {
			output[i] = Math.tanh(vec[i]);
		}

		return output;
	}

	public static double[][] sqrt(double[][] matrix) {
		double[][] output = new double[matrix.length][matrix[0].length];

		for(int i=0; i < matrix.length; i++) {
			output[i][i] = Math.sqrt(matrix[i][i]);
		}
		return output;
	}

	public static double[] sqrt(double[] vec) {
		double[] output = new double[vec.length];

		for(int i=0; i < vec.length; i++) {
			output[i] = Math.sqrt(vec[i]);
		}
		return output;
	}

	/*works*/
	public static double[][] outer(double[] vector1, double[] vector2) {
		double[][] output = new double[vector1.length][vector2.length];

		for(int i=0; i < vector1.length; i++) {
			double t = vector1[i];
			for(int j=0; j < vector2.length; j++) {
				output[i][j]=t*vector2[j];
			}
		}

		return output;
	}

	public static double[] multiply(double[] vector1, double d) {
		double[] output = new double[vector1.length];

		for(int i=0; i< vector1.length; i++) {
			output[i] = d*vector1[i];
		}

		return output;
	}

	public static double[][] multiply(double[][] matrix1, double d) {
		double[][] output = new double[matrix1.length][matrix1[0].length];

		for(int i=0; i< matrix1.length; i++) {
			for(int j=0; j < matrix1[0].length; j++) {
				output[i][j] = d*matrix1[i][j];				
			}
		}

		return output;
	}

	public static double dot_product(double[] vec1, double[] vec2) {
		double d = 0;

		for(int i=0; i < vec1.length; i++) {
			d += vec1[i]*vec2[i];
		}

		return d;
	}

	public static double[] square(double[] vec) {
		double[] output = new double[vec.length];

		for(int i=0; i < vec.length; i++) {
			output[i] = vec[i]*vec[i];
		}

		return output;
	}

	public static double[] add(double[] vec1, double[] vec2) {
		double[] output = new double[vec1.length];

		for(int i=0; i < vec1.length; i++) {
			output[i] = vec1[i]+vec2[i];
		}

		return output;
	}

	public static double[] add(double[] vec, double delta) {
		double[] output = new double[vec.length];

		for(int i=0; i < vec.length; i++) {
			output[i] = vec[i] + delta;
		}

		return output;
	}

	public static double[][] add(double[][] mat1, double[][] mat2) {
		double[][] output = new double[mat1.length][mat1[0].length];

		//System.out.println(mat1.length+" "+mat2.length+" "+mat1[0].length+" "+mat2[0].length);

		for(int i=0; i < mat1.length; i++) {
			for(int j=0; j< mat1[0].length; j++) {
				output[i][j] = mat1[i][j] + mat2[i][j];
			}
		}

		return output;
	}

	public static double[] minus(double[] vec1, double[] vec2) {
		double[] output = new double[vec1.length];

		for(int i=0; i < vec1.length; i++) {
			output[i] = vec1[i] - vec2[i];
		}

		return output;
	}

	public static double[] divide(double[] vec1, double[] vec2) {
		double[] output = new double[vec1.length];

		for(int i=0; i < vec1.length; i++) {
			output[i] = vec1[i] / vec2[i];
		}

		return output;
	}

	public static void main(String[] args) {
		double[] v1 = {1,2,3};
		double[] v2 = {11,12,13};
		double[] v3 = {21,22,23,24};

		double[][] m1 = {{1,2,3},{2,3,4},{3,4,5},{4,5,6}};

		double[][] t = outer(v1,v3);

		printMatrix(m1);
		//for(int i=0; i < t.length; i++) {
		//	String temp = "";
		//	for(int j=0; j<t[0].length; j++) {
		//		temp = temp + t[i][j]+" ";
		//	}
		//	System.out.println(temp);
		//}

		printVector(VMUtils.matrixVector(m1, v1));
	}

	public static void printVector(double[] d) {
		for(int i=0; i<d.length; i++) {
			System.out.println(d[i]);
		}
	}

	public static void printMatrix(double[][] d) {
		for(int i=0; i< d.length; i++) {
			String s ="";
			for(int j=0; j < d[0].length; j++) {
				s += d[i][j]+" ";
			}
			System.out.println(s);
		}
	}

}
