package edu.illinois.cs.learning;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import edu.illinois.cs.cogcomp.core.utilities.ResourceManager;
import edu.illinois.cs.readers.Example;
import edu.illinois.cs.utils.LearningUtils;
import edu.stanford.nlp.util.Pair;

public class WeightVector implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -503199673071492006L;
	static final double eps = 0.01;
    static final Random random = new Random(1);
    public double[] w = new double[120000];
    public ResourceManager rm;
    
    public WeightVector(ResourceManager rm) {
    	this.rm = rm;
    	//if(rm.getBoolean("randominit")) {
    	//	for(int i=0; i < w.length; i++) {
    	//		w[i]=2*random.nextDouble() -1;
    	//		w[i]=0.25*w[i];
    	//	}
    	//}
    }

	/**
     * Performs inference for a list of data points by collecting features from the {@link ILPAligner}.
     * @param aligner Performs the latent alignment between Hypothesis and Text nodes/edges
     * @param testData The test data
     * @return The prediction for each data point (1 for Entailment, -1 for Contradiction/Unknown)
     */
    public double[] predictScoreByFindingFeatures(Aligner aligner, List<Example> testData) {
        int size = testData.size();
        double[] scores = new double[size];
        for (int i = 0; i < size; i++) {
            //if (i % 100 == 0)
            //    System.out.println("Predicting: " + i + "/" + size);
            Feature[] fn = aligner.getBestFeatures(testData.get(i), this);
            scores[i] = dotProduct(fn);
        }
        return scores;
    }

    public void setWElement(int index, double value) {
        if (index < w.length)
            w[index] = value;
        else {
            int new_length = w.length * 2;
            double[] new_w = new double[new_length];
            if(rm.getBoolean("randominit")) {
        		for(int i=0; i < w.length; i++) {
        			new_w[i]=2*random.nextDouble() -1;
        			new_w[i]=0.25*new_w[i];
        		}
            }
            
            System.arraycopy(w, 0, new_w, 0, w.length);

            new_w[index] = value;

            w = new_w;
        }
    }

    public double dotProduct(Feature[] fn) {
        double res = 0.0;

        for (Feature f : fn) {
            // test data might contain some new features
            if (f.getIndex() <= w.length)
                res += w[f.getIndex() - 1] * f.getValue();
            else
                // if this feature is not in the feature space, enlarge the "w"
                setWElement(f.getIndex() - 1, 0.0);
        }

        return res;
    }

    /**
     * Trains an SVM using the {@link ILPAligner} as a latent structure.
     * @param aligner Performs the latent alignment between Hypothesis and Text nodes/edges
     * @param trainLabels The gold labels, reduced to 1 (Entailment) and -1 (Contradiction/Unknown)
     * @param trainingData The training data
     * @param outerIters Maximum number of outer iterations
     * @param C SVM's constant
     * @param test_data 
     */
    public void updateWeightVectorWithFullCDLatentSVM(Aligner aligner, List<Integer> trainLabels,
                                                      List<Example> trainingData, int outerIters, double C, ArrayList<Example> test_data, ArrayList<Example> dev_data) {
        List<Map<String, Pair<Feature[], Double>>> negative_x_collections = new ArrayList<Map<String, Pair<Feature[], Double>>>();
        List<Double> positive_x_alpha = new ArrayList<Double>();

        // initialize neg collection
        for (int i = 0; i < trainingData.size(); i++) {
            positive_x_alpha.add(0.0);
            if (trainLabels.get(i) == 1) {
                negative_x_collections.add(null);
            } else {
                negative_x_collections.add(new HashMap<String, Pair<Feature[], Double>>());
            }
        }

        // start training
        double out_pre_obj = Double.POSITIVE_INFINITY; // outer loop: minimize
        double out_obj;

        for (int k = 0; k < outerIters; k++) {
            // update positive examples
            //System.out.println("iter: " + k);
            List<Feature[]> positive_x = new ArrayList<Feature[]>();

            for (int i = 0; i < trainingData.size(); i++) {
                if (trainLabels.get(i) == 1) {
                    Feature[] bestFeatures = aligner.getBestFeatures(trainingData.get(i), this);
                    positive_x.add(bestFeatures);
                } else
                    positive_x.add(null);
            }

            // update negative collections
            double pre_obj = Double.NEGATIVE_INFINITY;
            double obj;
            int inner_loop = 0;

            while (true) {
                int n_new = 0;
                for (int i = 0; i < trainingData.size(); i++) {
                    if (trainLabels.get(i) == -1) {
                        // first check if
                        Feature[] x_single = aligner.getBestFeatures(trainingData.get(i), this);

                        String fstr = "";
                        for (Feature fn : x_single) {
                            fstr += (fn.getIndex()) + ":" + fn.getValue() + " ";
                        }

                        if (!negative_x_collections.get(i).containsKey(fstr)) {
                            n_new += 1;
                            negative_x_collections.get(i).put(fstr, new Pair<Feature[], Double>(x_single, 0.0));
                        }
                    }
                }

                //System.out.println("In the inner loop " + inner_loop + ": Add " + n_new + " negative examples..");

                // it we do not get any new neg
                obj = updateWeightVectorWithWorkingSetCDSVM(trainLabels, C, negative_x_collections, positive_x, positive_x_alpha);

                double inner_stop = Math.abs(pre_obj - obj) / Math.abs(obj);
                if (n_new == 0 && inner_stop < 1e-5) {
                    //System.out.println("Met the stopping condition; Exit Inner loop");
                    break;
                }
                pre_obj = obj;
                inner_loop++;
                
                //evaluate
                //double[] testScore = predictScoreByFindingFeatures(aligner, test_data);
                //Main.evaluate(test_data, testScore);
                
                //added by jack
                //if(inner_loop > 500)
                //	break;
            }
            out_obj = obj;

            double outer_stop = Math.abs(out_pre_obj - out_obj) / Math.abs(out_obj);
            System.out.println("outer_stop: " + outer_stop);
            if (outer_stop < 1e-5) {
                //System.out.println("Met the stopping condition; Exit Outer loop");
                //break;
            }

            out_pre_obj = out_obj;
            
			//evaluate
			if(k%1 == 0) {
				double[] testScore = predictScoreByFindingFeatures(aligner, test_data);
				Pair<Double,int[]> p = LearningUtils.evaluate(test_data, testScore);

				double[] devScore = predictScoreByFindingFeatures(aligner, dev_data);
				Pair<Double,int[]> p2 = LearningUtils.evaluate(dev_data, devScore);
				
				System.out.println("iter: "+k+" dev_accuracy\t"+p2.first+"\ttest_accuracy\t"+p.first);
			}
        }
    }

    private double updateWeightVectorWithWorkingSetCDSVM(List<Integer> y, double C,
                                                         List<Map<String, Pair<Feature[], Double>>> negative_x_collections,
                                                         List<Feature[]> positive_x, List<Double> positive_x_alpha) {
        final int SVM_ITER = 10000;
        // calculate n_features, n_examples
        int max_n = -1;

        for (int i = 0; i < y.size(); i++) {
            assert y.get(i) == 1 || y.get(i) == -1;
            if (y.get(i) == 1) {
                Feature[] featureNodes = positive_x.get(i);
                for (Feature fn : featureNodes)
                    if (fn.getIndex() > max_n)
                        max_n = fn.getIndex();
            }
            else {
                for (Pair<Feature[], Double> pair : negative_x_collections.get(i).values()) {
                    for (Feature fn : pair.first)
                        if (fn.getIndex() > max_n)
                            max_n = fn.getIndex();
                }
            }
        }

        double[] cur_w = new double[max_n];

        // restore previous w
        for (int i = 0; i < y.size(); i++) {
            if (y.get(i) == 1) {
                double alpha = positive_x_alpha.get(i);
                Feature[] fns = positive_x.get(i);
                for (Feature fn : fns) {
                    cur_w[fn.getIndex() - 1] += y.get(i) * alpha * fn.getValue();
                }
            }
            else {
                for (Pair<Feature[], Double> pair : negative_x_collections.get(i).values()) {
                    double alpha = pair.second;
                    Feature[] fns = pair.first;
                    for (Feature fn : fns) {
                        cur_w[fn.getIndex() - 1] += y.get(i) * alpha * fn.getValue();
                    }
                }
            }
        }

        int[] index = new int[y.size()];

        for (int i = 0; i < y.size(); i++)
            index[i] = i;

        double PGmax_new, PGmin_new;


        int t;
        for (t = 0; t < SVM_ITER; t++) {
            PGmax_new = Float.NEGATIVE_INFINITY;
            PGmin_new = Float.POSITIVE_INFINITY;

            for (int i = 0; i < y.size(); i++) {
                int j = i + random.nextInt(y.size() - i);
                int tmp = index[i];
                index[i] = index[j];
                index[j] = tmp;
            }

            if (t % 10 == 0) {
                System.out.print(".");
                System.out.flush();
            }

            for (int i = 0; i < y.size(); i++) {
                int idx = index[i];
                double sum_alpha_ij_over_j = 0;
                if (y.get(idx) == 1) {
                    double alpha = positive_x_alpha.get(idx);
                    assert alpha >= 0;
                    sum_alpha_ij_over_j = alpha;
                    Feature[] fns = positive_x.get(idx);

                    double dot_product = 0.0;
                    double xij_norm2 = 0.0;

                    for (Feature fn : fns) {
                        dot_product += cur_w[fn.getIndex() - 1] * fn.getValue();
                        xij_norm2 += fn.getValue() * fn.getValue();
                    }

                    double G = 1.0 - y.get(idx) * dot_product - sum_alpha_ij_over_j / (2.0 * C);

                    double PG = -G;
                    if (alpha == 0) {
                        PG = Math.min(-G, 0);
                    }
                    PGmax_new = Math.max(PGmax_new, PG);
                    PGmin_new = Math.min(PGmin_new, PG);

                    double step = G / (xij_norm2 + 1.0 / (2.0 * C));
                    // make sure alpha_[i][j] is alwasy greater than zero
                    step = Math.max(-alpha, step);

                    // update alpha
                    alpha += step;

                    for (Feature fn : fns) {
                        cur_w[fn.getIndex() - 1] += y.get(idx) * step * fn.getValue();
                    }
                    // restore alpha
                    assert (alpha >= -1e-5);

                    positive_x_alpha.set(idx, alpha);
                }
                else {
                    for (Pair<Feature[], Double> pair : negative_x_collections.get(idx).values()) {
                        sum_alpha_ij_over_j += pair.second;
                    }

                    for (Pair<Feature[], Double> pair : negative_x_collections.get(idx).values()) {
                        double dot_product = 0.0;
                        double xij_norm2 = 0.0;
                        Feature[] fns = pair.first;
                        double alpha = pair.second;
                        assert alpha >= 0;

                        for (Feature fn : fns) {
                            dot_product += cur_w[fn.getIndex() - 1] * fn.getValue();
                            xij_norm2 += fn.getValue() * fn.getValue();
                        }

                        double G = 1.0 - y.get(idx) * dot_product - sum_alpha_ij_over_j / (2.0 * C);

                        double PG = -G;
                        if (alpha == 0)
                            PG = Math.min(-G, 0);

                        PGmax_new = Math.max(PGmax_new, PG);
                        PGmin_new = Math.min(PGmin_new, PG);

                        double step = G / (xij_norm2 + 1.0 / (2.0 * C));

                        // make sure alpha_[i][j] is alwasy greater than zero
                        step = Math.max(-alpha, step);

                        // update alpha
                        alpha += step;

                        // update sum_alpha_ij_over_j
                        sum_alpha_ij_over_j += step;

                        for (Feature fn : fns) {
                            cur_w[fn.getIndex() - 1] += y.get(idx) * step * fn.getValue();
                        }
                        assert (alpha >= -1e-5);
                        pair.setSecond(alpha);
                    }
                }
            }

            if (PGmax_new - PGmin_new <= eps) {
                break;
            }
        }

        double obj = 0;

        for (int i = 0; i < max_n; i++)
            obj += cur_w[i] * cur_w[i] * 0.5;

        for (int i = 0; i < y.size(); i++) {

            double sum_alpha_ij_over_j = 0;
            if (y.get(i) == 1)
                sum_alpha_ij_over_j = positive_x_alpha.get(i);
            else {
                for (Pair<Feature[], Double> pair : negative_x_collections.get(i).values()) {
                    sum_alpha_ij_over_j += pair.second;
                }
            }

            obj -= sum_alpha_ij_over_j;
            obj += (1d / (4d * C)) * sum_alpha_ij_over_j * sum_alpha_ij_over_j;
        }

        System.out.println("In Iter " + t + " obj = " + obj);

        for (int i = 0; i < w.length; i++)
            w[i] = 0.0;

        for (int i = 0; i < cur_w.length; i++)
            setWElement(i, cur_w[i]);

        return obj;
    }
}
