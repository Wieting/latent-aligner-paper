package edu.illinois.cs.learning;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import edu.illinois.cs.features.Constituent;
import edu.illinois.cs.features.FeatureGenerator;
import edu.illinois.cs.readers.Example;
import edu.illinois.cs.utils.AlphanumComparator;

public class Alignment {

	public double score;
	public Map<Constituent, Constituent> nodeAlignmentFromHypothesisToText = new HashMap<Constituent, Constituent>();
	public Example e;
	public FeatureGenerator fea;
	
	public Alignment(Example e, FeatureGenerator fea) {
		this.e=e;
		this.fea = fea;
	}

	public Map<String, Double> getMapFeatures(FeatureGenerator fea) {
        Map<String, Double> featureMap = new HashMap<String, Double>();

        for (Constituent hypothesisNode : nodeAlignmentFromHypothesisToText.keySet()) {
            Constituent textNode = nodeAlignmentFromHypothesisToText.get(hypothesisNode);

            Map<String, Double> nodeFeatures = fea.getNodeAlignmentFeatures(e, textNode, hypothesisNode);
            addMapsInPlace(featureMap, nodeFeatures);
        }

        double normalize_factor = e.s_constituents.size();

        for (String key : featureMap.keySet()) {
            double value = featureMap.get(key) / normalize_factor;
            featureMap.put(key, value);
        }
        return featureMap;
	}

    private void addMapsInPlace(Map<String, Double> accumulator, Map<String, Double> toAdd) {
        for (String s : toAdd.keySet()) {
            if (!accumulator.containsKey(s))
                accumulator.put(s, toAdd.get(s));
            accumulator.put(s, accumulator.get(s) + toAdd.get(s));
        }
    }
	
	public void addNodeAlignmentFromHypothesisToText(Constituent c1,
			Constituent c2) {
		nodeAlignmentFromHypothesisToText.put(c1, c2);
	}
	
	public String toString() {
		ArrayList<String> lis = new ArrayList<String>();
		for(Constituent c: nodeAlignmentFromHypothesisToText.keySet()) {
			Constituent c2 = nodeAlignmentFromHypothesisToText.get(c);
			String ss = (c.start+" "+c.surfaceText+" "+c2.surfaceText);
			Map<String, Double> map = fea.getNodeAlignmentFeatures(e, c2, c);
			ss = ss+" "+map2String(map);
			lis.add(ss);
		}
		
		return getString(lis);
	}

	public String map2String(Map<String,Double> map) {
		String t = "";
		
		for(String s : map.keySet()) {
			t = t +" "+s+":"+map.get(s);
		}
		
		t = t.trim();
		
		return t;
	}
	
	public String getString(ArrayList<String> lines) {
		Collections.sort(lines, new AlphanumComparator());
		String ss ="";
		for(String s: lines) {
			ss = ss + s + "\n";
		}
		
		return ss;
	}
}
