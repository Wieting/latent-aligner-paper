package edu.illinois.cs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;

import edu.illinois.cs.features.Constituent;
import edu.illinois.cs.features.Ngrams;
import edu.illinois.cs.features.PPDB;
import edu.illinois.cs.features.PPDBcontainer;
import edu.illinois.cs.readers.Example;
import edu.illinois.cs.utils.Utils;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.Pair;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class MainTest
    extends TestCase
{
	
	public StanfordCoreNLP pipeline;
	public PPDB ppdb;
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MainTest( String testName )
    {
        super( testName );
		Properties props = new Properties();
		props.put("annotators", "tokenize, ssplit, pos, lemma");
		props.put("tokenize.options", "americanize=true");
		props.put("ssplit.isOneSentence", "true");
		pipeline = new StanfordCoreNLP(props);
		ppdb = new PPDB("data/resources/ppdb_test_lex.txt","data/resources/ppdb_test_o2m.txt","data/resources/ppdb_test_phr.txt", pipeline);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( MainTest.class );
    }
    
    
    public boolean checkStringLists(ArrayList<String> l1, ArrayList<String> l2) {
    	if(l1.size() != l2.size())
    		return false;
    	for(String s: l1) {
    		if(l2.contains(s))
    			continue;
    		else
    			return false;
    	}
    	
    	return true;
    }
    
    public void testLexPPDBFeatures()
    {
    	ArrayList<String> l1 = new ArrayList<String>();
    	ArrayList<String> l2 = new ArrayList<String>();
    	
    	String s1 = "The fishery kneel payback the dog.";
    	String s2 = "My crouch was good and the Harvest was vengeance";
    	Example e = new Example("test_corpus",0,1,"ENTAILMENT",s1,s2,pipeline,true);
    	PPDBcontainer pc = ppdb.analyzeSentencePair(e);
    	ArrayList<Pair<Constituent,Constituent>> consmap = pc.lexmap;
    	l1.add("fishery 1 Harvest 6");
    	l1.add("kneel 2 crouch 1");
    	l1.add("payback 3 vengeance 8");
    	for(Pair<Constituent,Constituent> p : consmap) {
			Constituent c1 = p.first();
			Constituent c2 = p.second();
    		//System.out.println(c1.surfaceText+" "+c1.start+" "+c2.surfaceText+" "+c2.start);
			l2.add(c2.surfaceText+" "+c2.start+" "+c1.surfaceText+" "+c1.start);
    	}
        assertTrue( checkStringLists(l1,l2) );
    }
}
