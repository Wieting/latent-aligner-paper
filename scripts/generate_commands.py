word_sims = ['ppdb','paragram1.0','paragram2.0','glove_25_3','glove_25_5','glove_25_10','glove_25_15','glove_25_1',
             'w2vec_25_3','w2vec_25_5','w2vec_25_10','w2vec_25_15','w2vec_25_1','cc','cc_lowercase']

cmd1 = 'java -Xmx1g -cp target/dependency/*:target/latent-aligner-release-1.0-SNAPSHOT.jar edu.illinois.cs.main.Main config/learn.ent.txt'
cmd2 = 'java -Xmx1g -cp target/dependency/*:target/latent-aligner-release-1.0-SNAPSHOT.jar edu.illinois.cs.main.Main config/learn.sim.txt'
cmd3 = 'java -Xmx1g -cp target/dependency/*:target/latent-aligner-release-1.0-SNAPSHOT.jar edu.illinois.cs.main.Main config/learn.ent.noword.txt'
cmd4 = 'java -Xmx1g -cp target/dependency/*:target/latent-aligner-release-1.0-SNAPSHOT.jar edu.illinois.cs.main.Main config/learn.sim.noword.txt'

lrs = [0.01,0.05,0.10]

for r in lrs:
    r = str(r)
    print cmd1 +" null null false false "+r+" > job_output/wordonly."+r+".ent.out"
    print cmd2 +" null null false false "+r+" > job_output/wordonly."+r+".sim.out"

for i in word_sims:
    for r in lrs:
        r = str(r)
        cmd = cmd1+" ../latent-aligner-data/caches/"+i+".cache null false false "+r+" > job_output/"+i+"."+r+".ent.txt"
        print cmd
        cmd = cmd3+" ../latent-aligner-data/caches/"+i+".cache null false false "+r+" > job_output/"+i+"."+r+".ent.noword.txt"
        print cmd

for i in word_sims:
    for r in lrs:
        r = str(r)
        cmd = cmd2+" ../latent-aligner-data/caches/"+i+".cache null false false "+r+" > job_output/"+i+"."+r+".sim.txt"
        print cmd
        cmd = cmd4+" ../latent-aligner-data/caches/"+i+".cache null false false "+r+" > job_output/"+i+"."+r+".sim.noword.txt"
        print cmd
