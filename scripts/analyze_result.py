import glob
ents = []
for name in glob.glob('../ent_*'):
    ents.append(name)

sims = []
for name in glob.glob('../sim_*'):
    sims.append(name)

for ff in ents:
    f=open(ff,'r')
    lines = f.readlines()
    best_dev = 0
    best_line = ""
    best_test = 0
    best_line_test = 0
    for i in lines:
        i=i.strip()
        if('iter:' in i):
            arr = i.split()
            devscore = float(arr[3])
            testscore = float(arr[5])
            if(devscore > best_dev):
                best_dev = devscore
                best_line = arr[1]+" "+arr[3]+" "+arr[5]
            if(testscore > best_test):
                best_test = 0
                best_line_test = arr[1]+" "+arr[3]+" "+arr[5]
    print ff+"\t"+ best_line+"\t"+best_line_test

print ""

for ff in sims:
    f=open(ff,'r')
    lines = f.readlines()
    best_dev = 0
    best_line = ""
    best_test = 0
    best_line_test = 0
    for i in lines:
        i=i.strip()
        if('iter:' in i):
            arr = i.split()
            devscore = float(arr[3])
            testscore = float(arr[5])
            if(devscore > best_dev):
                best_dev = devscore
                best_line = arr[1]+" "+arr[3]+" "+arr[5]
            if(testscore > best_test):
                best_test = 0
                best_line_test = arr[1]+" "+arr[3]+" "+arr[5]
    print ff+"\t"+ best_line+"\t"+best_line_test